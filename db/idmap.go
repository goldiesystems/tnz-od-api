package db

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"

	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
	"bitbucket.org/goldiesystems/tnzodapi/model"
)

const idmapCol = "idmap"

// StoreIDMap stores the ID mapping data to the database.
func StoreIDMap(coreID int, listingID int) error {
	col := database.Collection(idmapCol)
	filter, update := prepareMap(coreID, listingID)
	res, err := col.UpdateOne(
		ctx,
		filter,
		update,
		options.Update().SetUpsert(true),
	)
	if err != nil {
		logger.Error.Println(err)
		return err
	}
	if res.UpsertedCount > 0 {
		logger.Trace.Printf("Stored idmap(%d, %d) to database\n", coreID, listingID)
	}

	return nil
}

// LoadIDMap retrieves all of the ID mapping data from the database
func LoadIDMap() (map[int]int, error) {
	m := map[int]int{}
	col := database.Collection(idmapCol)
	filter := bson.M{}

	cursor, err := col.Find(ctx, filter)
	if err != nil {
		logger.Error.Println(err)
		return m, err
	}

	for cursor.Next(ctx) {
		var mapper model.IDMapper
		err = cursor.Decode(&mapper)
		if err != nil {
			logger.Error.Println(err)
			return m, err
		}
		m[mapper.CoreID] = mapper.ListingID
	}

	if err = cursor.Err(); err != nil {
		logger.Error.Println(err)
		return m, err
	}

	return m, nil
}

func prepareMap(key int, value int) (bson.M, bson.M) {
	filter := bson.M{"coreID": key}
	update := bson.M{"$set": bson.M{"listingID": value}}

	return filter, update
}
