package db

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"

	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
	"bitbucket.org/goldiesystems/tnzodapi/model"
)

const compCol = "comp"

// ClearComparisons removes all of the existing comparison records from the
// database
func ClearComparisons(version int) error {
	colName := fmt.Sprintf("%s%d", compCol, version)
	return deleteAll(colName)
}

// StoreComparison stores comparison records in the database
func StoreComparison(c model.Comparison, version int) error {
	colName := fmt.Sprintf("%s%d", compCol, version)
	col := database.Collection(colName)
	filter, update := prepareComparison(c)
	_, err := col.UpdateOne(
		ctx,
		filter,
		update,
		options.Update().SetUpsert(true),
	)
	if err != nil {
		logger.Error.Println(err)
		return err
	}
	return nil
}

// GetComparisonCount retrieves the number of comparison records stored for the
// given API version
func GetComparisonCount(version int) (int, error) {
	colName := fmt.Sprintf("%s%d", compCol, version)
	col := database.Collection(colName)
	filter := bson.M{}

	// Get total items in collection
	nItems, err := col.CountDocuments(ctx, filter)
	if err != nil {
		logger.Error.Println(err)
		return 0, err
	}
	
	// Conversion down but unlikely to truncate on our dataset
	return int(nItems), nil
}

// LoadComparisons retrieves all comparison records for the given "page".
// The int returned after the slice is the total number of records in the
// slice, while the int64 value is the total number of records stored in
// the database collection.
func LoadComparisons(
	version int,
	skip int,
	limit int,
) ([]model.Comparison, error) {

	var c []model.Comparison

	colName := fmt.Sprintf("%s%d", compCol, version)
	col := database.Collection(colName)
	filter := bson.M{}

	// Get the requested "page" of records
	longSkip := int64(skip)
	longLimit := int64(limit)
	opt := options.FindOptions{
		Skip:  &longSkip,
		Limit: &longLimit,
	}
	cursor, err := col.Find(ctx, filter, &opt)
	if err != nil {
		logger.Error.Println(err)
		return c, err
	}
	for cursor.Next(ctx) {
		var comp model.Comparison
		err = cursor.Decode(&comp)
		if err != nil {
			logger.Error.Println(err)
			return c, err
		}
		c = append(c, comp)
	}

	return c, nil
}

// LoadComparison retrieves a comparison record for the given key.
func LoadComparison(version int, key string) (model.Comparison, error) {
	var c model.Comparison
	var err error

	colName := fmt.Sprintf("%s%d", compCol, version)
	col := database.Collection(colName)
	filter := bson.M{"key": key}
	err = col.FindOne(ctx, filter).Decode(&c)
	return c, err
}

func prepareComparison(c model.Comparison) (bson.M, bson.M) {
	filter := bson.M{"key": c.Key}
	update := bson.M{"$set": c}
	return filter, update
}
