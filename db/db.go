package db

import (
	"context"
	"fmt"
	"os"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
)

var client *mongo.Client
var connStr string
var ctx context.Context
var database *mongo.Database

func init() {
	host := os.Getenv("MONGO_HOST")
	port := os.Getenv("MONGO_PORT")
	user := os.Getenv("MONGO_USER")
	password := os.Getenv("MONGO_PASSWORD")
	dbName := os.Getenv("MONGO_DATABASE")

	connStr = fmt.Sprintf(
		"mongodb://%s:%s@%s:%s/%s",
		user,
		password,
		host,
		port,
		dbName,
	)
}

// Connect establishes a connection to the global default database and
// ensures indexes are set on collections.
func Connect(c context.Context) error {
	ctx = c
	var err error
	client, err = mongo.NewClient(options.Client().ApplyURI(connStr))
	if err != nil {
		return err
	}
	err = client.Connect(ctx)
	if err != nil {
		return err
	}
	database = client.Database(os.Getenv("MONGO_DATABASE"))

	return ensureIndexes()
}

// Disconnect removes the connect to the global default database
func Disconnect() error {
	return client.Disconnect(ctx)
}

func ensureIndexes() error {
	// Ensure idmap index is set
	col := database.Collection(idmapCol)
	im := mongo.IndexModel{
		Keys:    bson.M{"coreID": 1},
		Options: options.Index().SetUnique(true),
	}
	logger.Trace.Printf("Setting mongodb collection index for %s\n", idmapCol)
	_, err := col.Indexes().CreateOne(ctx, im, nil)
	return err
}

func deleteAll(colName string) error {
	logger.Info.Printf("Removing all records from %s", colName)

	col := database.Collection(colName)
	filter := bson.M{}
	res, err := col.DeleteMany(ctx, filter)
	if err != nil {
		logger.Error.Println(err)
		return err
	}

	logger.Info.Printf("Removed %d records\n", res.DeletedCount)
	return err
}
