package db

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson"

	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
	"bitbucket.org/goldiesystems/tnzodapi/model"
)

// LoadCallSet retrieves the last call set from the database for the given
// environment and version.
func LoadCallSet(environment string, version int) ([]model.CallSet, error) {
	var cs []model.CallSet
	col := database.Collection(callSetCollectionName(environment, version))
	filter := bson.M{}

	cursor, err := col.Find(ctx, filter)
	if err != nil {
		logger.Error.Println(err)
		return cs, err
	}

	for cursor.Next(ctx) {
		var callSet model.CallSet
		if err = cursor.Decode(&callSet); err != nil {
			logger.Error.Println(err)
			return cs, err
		}
		cs = append(cs, callSet)
	}

	if err = cursor.Err(); err != nil {
		logger.Error.Println(err)
		return cs, err
	}

	return cs, nil
}

// ClearCallSet removes all of the call set records from the databaes for the
// given environment and version
func ClearCallSet(environment string, version int) error {
	return deleteAll(callSetCollectionName(environment, version))
}

// StoreCallSet inserts a single region-market pair into the database
func StoreCallSet(cs model.CallSet, environment string, version int) error {
	col := database.Collection(callSetCollectionName(environment, version))
	_, err := col.InsertOne(ctx, cs)
	if err != nil {
		logger.Error.Println(err)
		return err
	}
	logger.Trace.Printf(
		"Stored call set {%s, %s} to database\n",
		cs.Region,
		cs.Market,
	)
	return nil
}

func callSetCollectionName(environment string, version int) string {
	return fmt.Sprintf("callset%sv%d", environment, version)
}
