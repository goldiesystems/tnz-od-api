package db

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
	"bitbucket.org/goldiesystems/tnzodapi/model"
)

// ClearProducts removes all of the products for the given environment and
// version from the database.
func ClearProducts(environment string, version int) error {
	return deleteAll(productTableName(environment, version))
}

// StoreProducts stores the given products in the database
func StoreProducts(products []model.Product, environment string, version int) error {

	// Check for nothing to do
	if len(products) == 0 {
		return nil
	}

	// Bulk insert new products
	colName := productTableName(environment, version)
	logger.Info.Printf("Preparing to store %d products into %s\n", len(products), colName)
	col := database.Collection(colName)
	var writes []mongo.WriteModel
	for _, p := range products {
		f, u := prepareProductRecord(p)
		m := mongo.NewUpdateOneModel().SetUpsert(true).SetFilter(f).SetUpdate(u)
		writes = append(writes, m)
	}
	_, err := col.BulkWrite(ctx, writes)
	if err != nil {
		logger.Error.Println(err)
		return err
	}

	count, err := col.CountDocuments(ctx, bson.M{})
	if err != nil {
		logger.Error.Println(err)
	}
	logger.Info.Printf("%d records stored in %s\n", count, colName)

	return err
}

// GetProductCount retrieves the number of product records stored for the given
// environment and API version
func GetProductCount(environment string, version int) (int, error) {
	colName := productTableName(environment, version)
	col := database.Collection(colName)
	filter := bson.M{}

	nItems, err := col.CountDocuments(ctx, filter)
	if err != nil {
		logger.Error.Println(err)
		return 0, err
	}

	// Conversion down but unlikely to truncate on our dataset
	return int(nItems), nil
}

// LoadProducts reads previously imported products from the database
func LoadProducts(environment string, version int) (map[string]model.Product, error) {

	p := make(map[string]model.Product)

	colName := productTableName(environment, version)
	col := database.Collection(colName)
	filter := bson.M{}
	cursor, err := col.Find(ctx, filter)
	if err != nil {
		logger.Error.Println(err)
		return p, err
	}
	for cursor.Next(ctx) {
		var prod model.Product
		err = cursor.Decode(&prod)
		if err != nil {
			logger.Error.Println(err)
			return p, err
		}
		var key string
		if prod.RefID == 0 {
			key = fmt.Sprintf("%d_%s", prod.ODListingOID, prod.Market)
		} else {
			key = fmt.Sprintf("%d_%s", prod.RefID, prod.Market)
		}
		p[key] = prod
	}

	return p, nil

}

// ClearProductErrors removes all known data issues from the database.
func ClearProductErrors(environment string, version int) error {
	return deleteAll(productErrorTableName(environment, version))
}

// StoreProductErrors stores known data issues and corresponding URL to the
// database.
func StoreProductErrors(pe []model.ProductError, environment string, version int) error {
	colName := productErrorTableName(environment, version)
	col := database.Collection(colName)
	for _, e := range pe {
		_, err := col.InsertOne(ctx, e)
		if err != nil {
			logger.Error.Println(err)
			return err
		}
	}
	return nil
}

// GetProductErrorCount retrieves the number of product error records stored
// for the given environment and API version
func GetProductErrorCount(environment string, version int) (int, error) {
	colName := productErrorTableName(environment, version)
	col := database.Collection(colName)
	filter := bson.M{}

	nItems, err := col.CountDocuments(ctx, filter)
	if err != nil {
		logger.Error.Println(err)
		return 0, err
	}

	// Conversion down but unlikely to truncate on our dataset
	return int(nItems), nil
}

func prepareProductRecord(p model.Product) (bson.M, bson.M) {
	filter := bson.M{
		"od_listing_o_id": p.ODListingOID,
		"market":          p.Market,
	}
	update := bson.M{"$set": p}

	return filter, update
}

func productTableName(environment string, version int) string {
	return fmt.Sprintf("%sv%d", environment, version)
}

func productErrorTableName(environment string, version int) string {
	return fmt.Sprintf("errors%sv%d", environment, version)
}
