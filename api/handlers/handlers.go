package handlers

import (
	"net/http"

	"bitbucket.org/goldiesystems/tnzodapi/internal/config"

	"github.com/go-chi/render"
)

var cfg config.Config

// Configure sets the package level configuration struct
func Configure(c config.Config) {
	cfg = c
}

func errorResponse(res http.ResponseWriter, req *http.Request, err error) {
	render.Status(req, http.StatusInternalServerError)
	response := struct {
		Message string `json:"message"`
	}{err.Error()}
	render.JSON(res, req, response)
}

func notFoundResponse(res http.ResponseWriter, req *http.Request) {
	render.Status(req, http.StatusNotFound)
	response := struct {
		Message string `json:"message"`
	}{"Resource not found"}
	render.JSON(res, req, response)
}

// PingResponse returns a simple 200 response with links to available resources
func PingResponse(res http.ResponseWriter, req *http.Request) {
	render.Status(req, http.StatusOK)
	var links []map[string]string
	var summary = map[string]string{
		"href": "/summary",
		"rel":  "summary",
		"type": "GET",
	}
	links = append(links, summary)
	var comp = map[string]string{
		"href": "/comparisons",
		"rel":  "comparisons",
		"type": "GET",
	}
	links = append(links, comp)

	response := struct {
		Message string              `json:"message"`
		Links   []map[string]string `json:"links"`
	}{
		Message: "OK",
		Links:   links,
	}
	render.JSON(res, req, response)
}
