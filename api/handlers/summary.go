package handlers

import (
	"net/http"

	"bitbucket.org/goldiesystems/tnzodapi/db"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// SummaryResponse is the object returned from requesting a data summary record
type SummaryResponse struct {
	V1      SummaryRecord `json:"v1"`
	V4      SummaryRecord `json:"v4"`
	Message string        `json:"message"`
}

// SummaryRecord is a set of counts for an API version
type SummaryRecord struct {
	Old   SummaryProduct `json:"old"`
	New   SummaryProduct `json:"new"`
	Diffs int            `json:"diffs"`
}

// SummaryProduct is a set of counts for an environment and API version
type SummaryProduct struct {
	Count  int `json:"count"`
	Errors int `json:"errors"`
}

// SummaryRoutes establishes a multiplexer for data summary recources
func SummaryRoutes() *chi.Mux {
	router := chi.NewRouter()
	router.Get("/", GetSummary)
	return router
}

// GetSummary renders a SummaryResponse after retrieving all of the count data
// from the database.
func GetSummary(res http.ResponseWriter, req *http.Request) {

	// Version 1
	count, err := db.GetProductCount("old", 1)
	if err != nil {
		errorResponse(res, req, err)
	}
	errCount, err := db.GetProductErrorCount("old", 1)
	if err != nil {
		errorResponse(res, req, err)
	}
	oldProd := SummaryProduct{
		Count:  count,
		Errors: errCount,
	}
	count, err = db.GetProductCount("new", 1)
	if err != nil {
		errorResponse(res, req, err)
	}
	errCount, err = db.GetProductErrorCount("new", 1)
	if err != nil {
		errorResponse(res, req, err)
	}
	newProd := SummaryProduct{
		Count:  count,
		Errors: errCount,
	}
	diffs, err := db.GetComparisonCount(1)
	if err != nil {
		errorResponse(res, req, err)
	}
	v1 := SummaryRecord{
		Old:   oldProd,
		New:   newProd,
		Diffs: diffs,
	}

	// Version 4
	count, err = db.GetProductCount("old", 4)
	if err != nil {
		errorResponse(res, req, err)
	}
	errCount, err = db.GetProductErrorCount("old", 4)
	if err != nil {
		errorResponse(res, req, err)
	}
	oldProd = SummaryProduct{
		Count:  count,
		Errors: errCount,
	}
	count, err = db.GetProductCount("new", 4)
	if err != nil {
		errorResponse(res, req, err)
	}
	errCount, err = db.GetProductErrorCount("new", 4)
	if err != nil {
		errorResponse(res, req, err)
	}
	newProd = SummaryProduct{
		Count:  count,
		Errors: errCount,
	}
	diffs, err = db.GetComparisonCount(4)
	if err != nil {
		errorResponse(res, req, err)
	}
	v4 := SummaryRecord{
		Old:   oldProd,
		New:   newProd,
		Diffs: diffs,
	}

	response := SummaryResponse{
		V1:      v1,
		V4:      v4,
		Message: "",
	}
	render.JSON(res, req, response)
}
