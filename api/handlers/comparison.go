package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/goldiesystems/tnzodapi/db"
	"bitbucket.org/goldiesystems/tnzodapi/model"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// ComparisonResponse is the object returned from requesting one or more
// comparison records.
type ComparisonResponse struct {
	Version int                 `json:"version"`
	Skip    int                 `json:"skip"`
	Limit   int                 `json:"limit"`
	Records int                 `json:"records"`
	Count   int                 `json:"count"`
	Message string              `json:"message"`
	Links   []map[string]string `json:"links"`
	Data    []model.Comparison  `json:"data"`
}

// ComparisonRoutes establishes a multiplexer for comparison resources
func ComparisonRoutes() *chi.Mux {
	router := chi.NewRouter()
	router.Get("/", GetComparisons)
	router.Get("/{key}", GetComparison)
	return router
}

// GetComparisons retrieves a page of comparisons from the data store. If
// skip query string parameter is missing, it will default to 0. If limit query
// string parameter is missing or is greater than the configured page size then
// the page size will default to the configured size. If version query
// parameter is missing it will default to 4.
func GetComparisons(res http.ResponseWriter, req *http.Request) {

	// Unpack version and pagination variables from query string
	version := 4
	if qversion, ok := req.URL.Query()["version"]; ok {
		if len(qversion) == 1 {
			version, _ = strconv.Atoi(qversion[0])
		}
		if version != 1 && version != 4 {
			version = 4
		}
	}
	skip := 0
	if qskip, ok := req.URL.Query()["skip"]; ok {
		if len(qskip) == 1 {
			skip, _ = strconv.Atoi(qskip[0])
		}
		if skip < 0 {
			skip = 0
		}
	}
	limit := cfg.PageSize
	if qlimit, ok := req.URL.Query()["limit"]; ok {
		if len(qlimit) == 1 {
			limit, _ = strconv.Atoi(qlimit[0])
		}
		if limit < 1 || limit > cfg.PageSize {
			limit = cfg.PageSize
		}
	}

	// Get number of records in database
	totalRecords, err := db.GetComparisonCount(version)
	if err != nil {
		errorResponse(res, req, err)
		return
	}

	// Get comparisons from database
	c, err := db.LoadComparisons(version, skip, limit)
	if err != nil {
		errorResponse(res, req, err)
		return
	}

	links := createLinks(version, skip, limit, totalRecords)

	// Response
	response := ComparisonResponse{
		Version: version,
		Skip:    skip,
		Limit:   limit,
		Records: len(c),
		Count:   totalRecords,
		Message: "",
		Data:    c,
		Links:   links,
	}
	render.JSON(res, req, response)
}

// GetComparison retrieves a single comparison from the data store. If
// version query parameter is missing, it will default to 4.
func GetComparison(res http.ResponseWriter, req *http.Request) {

	// Get comparison record key
	key := chi.URLParam(req, "key")

	// Unpack version variable
	version := 4
	if qversion, ok := req.URL.Query()["version"]; ok {
		if len(qversion) == 1 {
			version, _ = strconv.Atoi(qversion[0])
		}
		if version != 1 && version != 4 {
			version = 4
		}
	}

	// Get comparison from database
	c, err := db.LoadComparison(version, key)
	cwrap := make([]model.Comparison, 1)
	cwrap[0] = c
	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			notFoundResponse(res, req)
		} else {
			errorResponse(res, req, err)
		}
		return
	}

	// Response
	response := ComparisonResponse{
		Version: version,
		Records: 1,
		Message: "",
		Data:    cwrap,
	}
	render.JSON(res, req, response)
}

func createLinks(version int, skip int, limit int, totalRecords int) []map[string]string {
	var links []map[string]string

	linkSkip := skip - cfg.PageSize
	if linkSkip > 0 {
		var prev = map[string]string{
			"href": fmt.Sprintf("/comparisons/?version=%d&skip=%d&limit=%d", version, linkSkip, limit),
			"rel":  "previous",
			"type": "GET",
		}
		links = append(links, prev)
	}
	linkSkip = skip + cfg.PageSize
	if linkSkip <= totalRecords {
		var next = map[string]string{
			"href": fmt.Sprintf("/comparisions/?version=%d?skip=%d&limit=%d", version, linkSkip, limit),
			"rel":  "next",
			"type": "GET",
		}
		links = append(links, next)
	}

	return links
}
