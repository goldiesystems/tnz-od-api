package main

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/goldiesystems/tnzodapi/api/handlers"
	"bitbucket.org/goldiesystems/tnzodapi/db"
	"bitbucket.org/goldiesystems/tnzodapi/internal/config"
	"bitbucket.org/goldiesystems/tnzodapi/internal/file"
	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
)

var cfg config.Config

func init() {
	// Load configuration
	var err error
	cfg, err = config.Load()
	if err != nil {
		fmt.Println("Error loading configuration file.", err)
		panic(err)
	}

	// Configure logging
	logFileT := fmt.Sprintf("%stnz-api-[T].txt", cfg.Logdir)
	logFile, err := file.ReplaceFilenameWildcards(logFileT)
	if err != nil {
		fmt.Println("Unable to format log file", err)
		return
	}
	err = logger.InitAllFile(logFile)
	if err != nil {
		fmt.Println("Unable to initialise logger", err)
		return
	}
	if !cfg.Debug {
		logger.SuppressTrace()
	}
}

func main() {

	// Configure database
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	err := db.Connect(ctx)
	if err != nil {
		fmt.Println("Unabled to connect to database", err)
		return
	}
	defer db.Disconnect()

	fmt.Println("Starting API...")
	logger.Info.Println("Starting API server")

	// Configure router
	handlers.Configure(cfg)
	router := routes()

	fmt.Printf("Listening on port: %s\n", os.Getenv("API_PORT"))
	http.ListenAndServe(":"+os.Getenv("API_PORT"), router)
}

func routes() *chi.Mux {

	router := chi.NewRouter()
	router.Use(
		render.SetContentType(render.ContentTypeJSON),
		middleware.Logger,
		middleware.DefaultCompress,
		middleware.RedirectSlashes,
	)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		AllowCredentials: true,
		MaxAge:           300,
	})
	router.Use(c.Handler)

	router.Get("/", handlers.PingResponse)
	router.Route("/", func(r chi.Router) {
		r.Mount("/comparisons", handlers.ComparisonRoutes())
		r.Mount("/summary", handlers.SummaryRoutes())
	})

	return router
}
