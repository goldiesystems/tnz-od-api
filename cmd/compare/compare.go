package main

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
	"bitbucket.org/goldiesystems/tnzodapi/model"
)

type fieldInfo struct {
	v1        reflect.Value
	v2        reflect.Value
	typeField reflect.StructField
	tag       string
}

func compare(
	p1 map[string]model.Product,
	p2 map[string]model.Product,
) (map[string]model.Comparison, error) {

	comp := map[string]model.Comparison{}
	var err error

	// Scan old products p1 and compare with corresponding new product in p2
	for key, prod1 := range p1 {
		// Ignore products with known issues that will cause an invalid
		// comparison
		if prod1.ODAccountOID <= 0 || prod1.Message == "item missing" {
			continue
		}

		s1 := model.CreateSummary(prod1, "old")
		c := model.Comparison{Key: key, Old: s1}
		if prod2, ok := p2[key]; ok {
			s2 := model.CreateSummary(prod2, "old") // TODO: Change to new
			c.New = s2
			c.Diff = deepCompare(prod1, prod2)
			if err != nil {
				return comp, err
			}
		} else {
			c.New = model.ProductSummary{}
			c.Diff = nil
		}
		comp[key] = c
	}

	// Scan new products p2 looking for any that are not in old products p1
	for key, prod2 := range p2 {
		// Ignore products with known issues that will cause an invalid
		// comparison
		if prod2.ODAccountOID <= 0 || prod2.Message == "item missing" {
			continue
		}

		if _, ok := p1[key]; !ok {
			s2 := model.CreateSummary(prod2, "old") // TODO: change to new
			c := model.Comparison{
				Key: key,
				Old: model.ProductSummary{},
				New: s2,
			}
			comp[key] = c
		}
	}

	return comp, nil
}

// Explicitly compares two Product structs and records the differences.
// Note the implementation is deliberately long-handed to deal with
// business rules rather than using reflect.DeepEqual. However, it is helped
// along with reflection.
func deepCompare(
	p1 model.Product,
	p2 model.Product,
) []model.Difference {

	var diffs []model.Difference
	var diff model.Difference
	var isDiff bool

	if diff, isDiff = compareMultiString(p1, p2, "AdultRateLowPeriodLowPrice"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "MailingPostcode"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "ProximityToCoach"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "ProximityToTown"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore ODAccountOID
	// Ignore RowNumber
	if diff, isDiff = compareStringSlice(p1, p2, "ListingTypes"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "OperationalHours"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareInt(p1, p2, "WebsiteLinkIsPublic"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "AdultRateHighPeriodHighPrice"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "Parking"); isDiff {
		diffs = append(diffs, diff)
	}
	if assetDiffs, isDiff := compareAssetSets(p1, p2); isDiff {
		diffs = append(diffs, assetDiffs...)
	}
	if diff, isDiff = compareString(p1, p2, "Market"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "HighPeriodStart"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore ModifiedDate
	if diff, isDiff = compareString(p1, p2, "RegionName"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "MailingStateProvince"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareStringSlice(p1, p2, "Tags"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "WebsiteLink"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "BookingEmail"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "MailingCityName"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "LowPeriodEnd"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "Postcode"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "ListingNameKey"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "Suburb"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore UniqueID
	if diff, isDiff = compareStringSlice(p1, p2, "LanguagesSpoken"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "ChildRateHighPeriodHighPrice"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "Freephone"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "ChildRateHighPeriodLowPrice"); isDiff {
		diffs = append(diffs, diff)
	}
	if qmDiffs, isDiff := compareQualmarkV2(p1, p2); isDiff {
		diffs = append(diffs, qmDiffs...)
	}
	if diff, isDiff = compareMultiString(p1, p2, "AdultRateHighPeriodLowPrice"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "BusinessType"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "CancellationPolicy"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "CountryName"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore MailingLUCityOID
	if diff, isDiff = compareInt(p1, p2, "FaxIsPublic"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareStringSlice(p1, p2, "Awards"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareInt(p1, p2, "PricingHasHighLow"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore ODListingOID
	if diff, isDiff = compareMultiString(p1, p2, "Street"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareStringMap(p1, p2, "AllSocialProfiles"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "Rooms"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "MinimumAge"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareStringSlice(p1, p2, "LanguagesWritten"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "NameOfListing"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "City"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "WeatherConsiderations"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "Mobile"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "CityName"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "RefIDInt"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "DOCConcessionID"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "MailingStreet"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "StateProvince"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "Email"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "ListingDescription"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareInt(p1, p2, "EmailIsPublic"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareInt(p1, p2, "FreephoneIsPublic"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore ProfileImageOID
	if diff, isDiff = compareMultiString(p1, p2, "BookingPhone"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareStringSlice(p1, p2, "Amenities"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareInt(p1, p2, "IsISite"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "MailingSuburb"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "ChildRateLowPeriodHighPrice"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "MailingCountryName"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "MarketLabel"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "BookingLink"); isDiff {
		diffs = append(diffs, diff)
	}
	// TODO: Deals
	if diff, isDiff = compareMultiString(p1, p2, "ProximityToAirport"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareFloat(p1, p2, "Latitude"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore LUCountryOID
	if diff, isDiff = compareString(p1, p2, "LowPeriodState"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore ODBusinessTypeOID
	if diff, isDiff = compareFloat(p1, p2, "Longitude"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "Phone"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore LURegionOID
	if opDiffs, isDiff := compareOnlineProfiles(p1, p2); isDiff {
		diffs = append(diffs, opDiffs...)
	}
	if diff, isDiff = compareString(p1, p2, "MailingRegionName"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "Conditions"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareInt(p1, p2, "MailingIsPublic"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "OperatingMonths"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareInt(p1, p2, "IsKiwiSpecialist"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore MailingLURegionOID
	if diff, isDiff = compareMultiString(p1, p2, "ChildRateAge"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "ActivityDuration"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "ChildRateLowPeriodLowPrice"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareString(p1, p2, "MailingCity"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareInt(p1, p2, "PhoneIsPublic"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "AdultRateLowPeriodHighPrice"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareInt(p1, p2, "IsTour"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareInt(p1, p2, "UsePhysicalAddress"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "MaxCapacity"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "ListingSummary"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore LogoImageAssetOID
	// Ignore MailingLUCountryOID
	if diff, isDiff = compareStringMap(p1, p2, "QualmarkRatings"); isDiff {
		diffs = append(diffs, diff)
	}
	if diff, isDiff = compareMultiString(p1, p2, "OtherCharges"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore ODListingMarketOID
	if diff, isDiff = compareMultiString(p1, p2, "Fax"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore LUCityOID
	if diff, isDiff = compareMultiString(p1, p2, "HighPeriodEnd"); isDiff {
		diffs = append(diffs, diff)
	}
	// Ignore Message

	return diffs
}

func compareAssetSets(p1 model.Product, p2 model.Product) ([]model.Difference, bool) {
	var diffs []model.Difference

	isLogoDiff := false
	if logoDiffs, isLogoDiff := compareAssets(p1.Assets.Logo, p2.Assets.Logo, "logo"); isLogoDiff {
		diffs = append(diffs, logoDiffs...)
	}
	isMainDiff := false
	if mainDiffs, isMainDiff := compareAssets(p1.Assets.Main, p2.Assets.Main, "main"); isMainDiff {
		diffs = append(diffs, mainDiffs...)
	}
	isGalleryDiff := false
	if galleryDiffs, isGalleryDiff := compareAssets(p1.Assets.Gallery, p2.Assets.Gallery, "gallery"); isGalleryDiff {
		diffs = append(diffs, galleryDiffs...)
	}

	return diffs, (isLogoDiff || isMainDiff || isGalleryDiff)
}

func compareAssets(
	m1 map[string]model.Asset,
	m2 map[string]model.Asset,
	setLabel string,
) ([]model.Difference, bool) {

	isDiff := false
	var isFieldDiff bool
	var diffs []model.Difference
	var diff model.Difference

	if len(m1) != len(m2) {
		diffs = append(
			diffs,
			model.Difference{
				FieldTag: setLabel,
				OldValue: fmt.Sprintf("<map size %d>", len(m1)),
				NewValue: fmt.Sprintf("<map size %d>", len(m2)),
			},
		)
		return diffs, true
	}

	for key, a1 := range m1 {
		// Map keys may not match so need to manually search for OID match
		assetExists := false
		for _, a2 := range m2 {
			if a1.OID == a2.OID {
				// Fieldwise comparison of Assets
				if diff, isFieldDiff = compareMultiString(a1, a2, "Credit"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareString(a1, a2, "Market"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareBool(a1, a2, "Exists"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareFloat(a1, a2, "Longitude"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareMultiString(a1, a2, "Width"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if assetDiffs, isFieldDiff := compareAssetInstances(a1.Instances, a2.Instances, setLabel, key); isFieldDiff {
					diffs = append(diffs, assetDiffs...)
					isDiff = true
				}
				if diff, isFieldDiff = compareInt(a1, a2, "UniqueID"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareMultiString(a1, a2, "Height"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				// Ignore OID (we are already looking explicitly for it)
				// Ignore URL
				if diff, isFieldDiff = compareMultiString(a1, a2, "Description"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareString(a1, a2, "AssetType"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareMultiString(a1, a2, "Label"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareMultiString(a1, a2, "Caption"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareFloat(a1, a2, "Latitude"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareInt(a1, a2, "TypeOID"); isFieldDiff {
					diff.FieldTag = setLabel + "." + key + "." + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}

				assetExists = true
				break
			}
		}
		if !assetExists {
			isDiff = true
			diffs = append(
				diffs,
				model.Difference{
					FieldTag: setLabel + "." + key,
					OldValue: "<asset content>",
					NewValue: "",
				},
			)
		}
	}

	return diffs, isDiff
}

func compareAssetInstances(
	ai1 []model.AssetInstance,
	ai2 []model.AssetInstance,
	assetType string,
	assetKey string,
) ([]model.Difference, bool) {

	isDiff := false
	var isFieldDiff bool
	var diffs []model.Difference
	var diff model.Difference

	if len(ai1) != len(ai2) {
		diffs = append(
			diffs,
			model.Difference{
				FieldTag: assetType + "." + assetKey,
				OldValue: fmt.Sprintf("<instances size %d>", len(ai1)),
				NewValue: fmt.Sprintf("<instances size %d>", len(ai2)),
			},
		)
		return diffs, true
	}

	for _, i1 := range ai1 {
		// Match on Format field
		instanceExists := false
		for _, i2 := range ai2 {
			if i1.Format == i2.Format {
				// Fieldwise comparison of Assets

				// Ignore ProviderLabel
				if diff, isFieldDiff = compareMultiString(i1, i2, "Width"); isFieldDiff {
					diff.FieldTag = assetType + "." + assetKey + "[" + i1.Format + "]" + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareMultiString(i1, i2, "Height"); isFieldDiff {
					diff.FieldTag = assetType + "." + assetKey + "[" + i1.Format + "]" + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				// Ignore URL

				instanceExists = true
				break
			}
		}
		if !instanceExists {
			isDiff = true
			diffs = append(
				diffs,
				model.Difference{
					FieldTag: assetType + "." + assetKey + "[" + i1.Format + "]",
					OldValue: "<instance content>",
					NewValue: "",
				},
			)
		}
	}

	return diffs, isDiff
}

func compareQualmarkV2(p1 model.Product, p2 model.Product) ([]model.Difference, bool) {
	isDiff := false
	var isFieldDiff bool
	var diffs []model.Difference
	var diff model.Difference

	qmLabel := getFieldTag(p1, "QualmarkV2Rating") + "."
	q1 := p1.QualmarkV2Rating
	q2 := p2.QualmarkV2Rating

	if diff, isFieldDiff = compareString(q1, q2, "AwardSegment"); isFieldDiff {
		diff.FieldTag = qmLabel + diff.FieldTag
		diffs = append(diffs, diff)
		isDiff = true
	}
	if diff, isFieldDiff = compareBool(q1, q2, "Exists"); isFieldDiff {
		diff.FieldTag = qmLabel + diff.FieldTag
		diffs = append(diffs, diff)
		isDiff = true
	}
	if diff, isFieldDiff = compareString(q1, q2, "Rating"); isFieldDiff {
		diff.FieldTag = qmLabel + diff.FieldTag
		diffs = append(diffs, diff)
		isDiff = true
	}
	if diff, isFieldDiff = compareString(q1, q2, "Grade"); isFieldDiff {
		diff.FieldTag = qmLabel + diff.FieldTag
		diffs = append(diffs, diff)
		isDiff = true
	}
	if diff, isFieldDiff = compareString(q1, q2, "AwardSector"); isFieldDiff {
		diff.FieldTag = qmLabel + diff.FieldTag
		diffs = append(diffs, diff)
		isDiff = true
	}

	return diffs, isDiff
}

func compareOnlineProfiles(p1 model.Product, p2 model.Product) ([]model.Difference, bool) {
	isDiff := false
	var isFieldDiff bool
	var diffs []model.Difference
	var diff model.Difference

	opLabel := getFieldTag(p1, "OnlineProfiles") + "."
	op1 := p1.OnlineProfiles
	op2 := p2.OnlineProfiles

	if len(op1) != len(op2) {
		diffs = append(
			diffs,
			model.Difference{
				FieldTag: opLabel,
				OldValue: fmt.Sprintf("<instances size %d>", len(op1)),
				NewValue: fmt.Sprintf("<instances size %d>", len(op2)),
			},
		)
		return diffs, true
	}

	for _, i1 := range op1 {
		// Match on ProfileType field
		profileExists := false
		for _, i2 := range op2 {
			if i1.ProfileType == i2.ProfileType {
				// Fieldwise comparison of OnlineProfiles
				if diff, isFieldDiff = compareString(i1, i2, "ProfileType"); isFieldDiff {
					diff.FieldTag = opLabel + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				if diff, isFieldDiff = compareMultiString(i1, i2, "ProfileData"); isFieldDiff {
					diff.FieldTag = opLabel + diff.FieldTag
					diffs = append(diffs, diff)
					isDiff = true
				}
				profileExists = true
				break
			}
		}
		if !profileExists {
			isDiff = true
			diffs = append(
				diffs,
				model.Difference{
					FieldTag: opLabel + "[" + i1.ProfileType + "]",
					OldValue: string(i1.ProfileData),
					NewValue: "",
				},
			)
		}
	}

	return diffs, isDiff
}

func compareString(
	s1 interface{},
	s2 interface{},
	fieldName string,
) (model.Difference, bool) {

	var diff model.Difference
	isDiff := false

	f, err := getFieldInfo(s1, s2, fieldName)
	if err != nil {
		logger.Error.Println(err)
		return diff, isDiff
	}
	if f.v1.Kind() != reflect.String {
		logger.Error.Printf(
			"Asked to compare a string on field %s of kind %s\n",
			fieldName,
			f.v1.Kind().String(),
		)
		return diff, isDiff
	}

	if f.v1.String() != f.v2.String() {
		isDiff = true
		diff = model.Difference{
			FieldTag: f.tag,
			OldValue: f.v1.String(),
			NewValue: f.v2.String(),
		}
	}
	return diff, isDiff
}

func compareStringSlice(
	s1 interface{},
	s2 interface{},
	fieldName string,
) (model.Difference, bool) {

	var diff model.Difference
	isDiff := false

	f, err := getFieldInfo(s1, s2, fieldName)
	if err != nil {
		logger.Error.Println(err)
		return diff, isDiff
	}
	if f.v1.Kind() != reflect.Slice {
		logger.Error.Printf(
			"Asked to compare a slice of strings on field %s of kind %s\n",
			fieldName,
			f.v1.Kind().String(),
		)
		return diff, isDiff
	}

	// Check if everything in v1 is in v2. If the slices are the same length
	// this is sufficient (don't need to check the other way around)
	if f.v1.Len() == f.v2.Len() {
		for i := 0; i < f.v1.Len(); i++ {
			if f.v1.Index(i).Kind() != reflect.String {
				logger.Error.Printf(
					"Asked to compare a slice of strings on field %s but found %s in slice\n",
					fieldName,
					f.v1.Index(i).Kind().String(),
				)
				return diff, false
			}
			item := f.v1.Index(i).String()
			found := false
			for j := 0; !found && j < f.v2.Len(); j++ {
				if item == f.v2.Index(j).String() {
					found = true
				}
			}
			if !found {
				isDiff = true
				break
			}
		}
	} else {
		isDiff = true
	}
	if isDiff {
		var str1 strings.Builder
		var str2 strings.Builder
		str1.WriteByte('[')
		str2.WriteByte('[')
		for i := 0; i < f.v1.Len(); i++ {
			str1.WriteString(f.v1.Index(i).String())
			if i < f.v1.Len()-1 {
				str1.WriteString(", ")
			}
		}
		for j := 0; j < f.v2.Len(); j++ {
			str2.WriteString(f.v2.Index(j).String())
			if j < f.v2.Len()-1 {
				str2.WriteString(", ")
			}
		}
		str1.WriteByte(']')
		str2.WriteByte(']')
		diff = model.Difference{
			FieldTag: f.tag,
			OldValue: str1.String(),
			NewValue: str2.String(),
		}
	}

	return diff, isDiff
}

func compareStringMap(
	s1 interface{},
	s2 interface{},
	fieldName string,
) (model.Difference, bool) {

	var diff model.Difference
	isDiff := false

	f, err := getFieldInfo(s1, s2, fieldName)
	if err != nil {
		logger.Error.Println(err)
		return diff, isDiff
	}
	if f.v1.Kind() != reflect.Map {
		logger.Error.Printf(
			"Asked to compare a map of strings on field %s of kind %s\n",
			fieldName,
			f.v1.Kind().String(),
		)
		return diff, isDiff
	}

	// Check if everything in v1 is in v2. If the slices are the same length
	// this is sufficient (don't need to check the other way around)
	if f.v1.Len() == f.v2.Len() {
		for _, key1 := range f.v1.MapKeys() {
			if f.v1.MapIndex(key1).Kind() != reflect.String {
				logger.Error.Printf(
					"Asked to compare a map of strings on field %s but found %s in map at key %s\n",
					fieldName,
					f.v1.MapIndex(key1).Kind().String(),
					key1.String(),
				)
				return diff, false
			}
			found := false
			for _, key2 := range f.v2.MapKeys() {
				if key2.String() == key1.String() {
					if f.v2.MapIndex(key2).Kind() != reflect.String {
						logger.Error.Printf(
							"Asked to compare a map of strings on field %s but found %s in map at key %s\n",
							fieldName,
							f.v2.MapIndex(key2).Kind().String(),
							key2.String(),
						)
						return diff, false
					}
					if f.v1.MapIndex(key1).String() != f.v2.MapIndex(key2).String() {
						isDiff = true
					}
					found = true
					break
				}
			}
			if !found {
				isDiff = true
			}
			if isDiff {
				break
			}
		}
	} else {
		isDiff = true
	}
	if isDiff {
		var str1 strings.Builder
		var str2 strings.Builder
		str1.WriteByte('{')
		for i, key1 := range f.v1.MapKeys() {
			str1.WriteByte('"')
			str1.WriteString(key1.String())
			str1.WriteString(`": "`)
			str1.WriteString(f.v1.MapIndex(key1).String())
			str1.WriteByte('"')
			if i < f.v1.Len()-1 {
				str1.WriteString(", ")
			}
		}
		str1.WriteByte('}')
		str2.WriteByte('{')
		for j, key2 := range f.v2.MapKeys() {
			str2.WriteByte('"')
			str2.WriteString(key2.String())
			str2.WriteString(`": "`)
			str2.WriteString(f.v2.MapIndex(key2).String())
			str2.WriteByte('"')
			if j < f.v2.Len()-1 {
				str2.WriteString(", ")
			}
		}
		str2.WriteByte('}')
		diff = model.Difference{
			FieldTag: f.tag,
			OldValue: str1.String(),
			NewValue: str2.String(),
		}
	}

	return diff, isDiff
}

func compareMultiString(
	s1 interface{},
	s2 interface{},
	fieldName string,
) (model.Difference, bool) {

	var diff model.Difference
	isDiff := false

	f, err := getFieldInfo(s1, s2, fieldName)
	if err != nil {
		logger.Error.Println(err)
		return diff, isDiff
	}
	if f.v1.Kind() != reflect.String {
		logger.Error.Printf(
			"Asked to compare a number string on field %s of kind %s\n",
			fieldName,
			f.v1.Kind().String(),
		)
		return diff, isDiff
	}

	if f.v1.String() != f.v2.String() {
		// Absolute strings are different so see if they can be converted to
		// various supported types and compared.
		if b1, err := strconv.ParseBool(f.v1.String()); err == nil {
			if b2, err := strconv.ParseBool(f.v2.String()); err == nil {
				if b1 == b2 {
					return diff, isDiff
				}
			}
		}
		if f1, err := strconv.ParseFloat(f.v1.String(), 64); err == nil {
			if f2, err := strconv.ParseFloat(f.v2.String(), 64); err == nil {
				if f1 == f2 {
					return diff, isDiff
				}
			}
		}
		isDiff = true
		diff = model.Difference{
			FieldTag: f.tag,
			OldValue: f.v1.String(),
			NewValue: f.v2.String(),
		}
	}
	return diff, isDiff
}

func compareBool(
	s1 interface{},
	s2 interface{},
	fieldName string,
) (model.Difference, bool) {

	var diff model.Difference
	isDiff := false

	f, err := getFieldInfo(s1, s2, fieldName)
	if err != nil {
		logger.Error.Println(err)
		return diff, isDiff
	}
	if f.v1.Kind() != reflect.Bool {
		logger.Error.Printf(
			"Asked to compare a bool on field %s of kind %s\n",
			fieldName,
			f.v1.Kind().String(),
		)
		return diff, isDiff
	}

	if f.v1.Bool() != f.v2.Bool() {
		isDiff = true
		diff = model.Difference{
			FieldTag: f.tag,
			OldValue: strconv.FormatBool(f.v1.Bool()),
			NewValue: strconv.FormatBool(f.v2.Bool()),
		}
	}
	return diff, isDiff
}

func compareInt(
	s1 interface{},
	s2 interface{},
	fieldName string,
) (model.Difference, bool) {

	var diff model.Difference
	isDiff := false

	f, err := getFieldInfo(s1, s2, fieldName)
	if err != nil {
		logger.Error.Println(err)
		return diff, isDiff
	}
	if f.v1.Kind() != reflect.Int {
		logger.Error.Printf(
			"Asked to compare an int on field %s of kind %s\n",
			fieldName,
			f.v1.Kind().String(),
		)
		return diff, isDiff
	}

	if f.v1.Int() != f.v2.Int() {
		isDiff = true
		diff = model.Difference{
			FieldTag: f.tag,
			OldValue: strconv.FormatInt(f.v1.Int(), 10),
			NewValue: strconv.FormatInt(f.v2.Int(), 10),
		}
	}
	return diff, isDiff
}

func compareFloat(
	s1 interface{},
	s2 interface{},
	fieldName string,
) (model.Difference, bool) {

	var diff model.Difference
	isDiff := false

	f, err := getFieldInfo(s1, s2, fieldName)
	if err != nil {
		logger.Error.Println(err)
		return diff, isDiff
	}
	if f.v1.Kind() != reflect.Float64 {
		logger.Error.Printf(
			"Asked to compare a float on field %s of kind %s\n",
			fieldName,
			f.v1.Kind().String(),
		)
		return diff, isDiff
	}

	if f.v2.Float() != f.v2.Float() {
		isDiff = true
		diff = model.Difference{
			FieldTag: f.tag,
			OldValue: strconv.FormatFloat(f.v1.Float(), 'f', -1, 64),
			NewValue: strconv.FormatFloat(f.v2.Float(), 'f', -1, 64),
		}
	}
	return diff, isDiff
}

func getFieldInfo(
	s1 interface{},
	s2 interface{},
	fieldName string,
) (fieldInfo, error) {

	var fi fieldInfo

	if reflect.TypeOf(s1).Kind() != reflect.Struct {
		return fi, errors.New("First parameter passed to getFieldInfo is not a struct")
	}
	if reflect.TypeOf(s2).Kind() != reflect.Struct {
		return fi, errors.New("Second parameter passed to getFieldInfo is not a struct")
	}
	if reflect.TypeOf(s1).Name() != reflect.TypeOf(s2).Name() {
		return fi, errors.New("Types passed to getFieldInfo are not the same")
	}

	st1 := reflect.ValueOf(s1)
	st2 := reflect.ValueOf(s2)
	typeField, _ := st1.Type().FieldByName(fieldName)

	fi = fieldInfo{
		v1:        st1.FieldByName(fieldName),
		v2:        st2.FieldByName(fieldName),
		typeField: typeField,
		tag:       typeField.Tag.Get("json"),
	}
	return fi, nil
}

func getFieldTag(s interface{}, fieldName string) string {
	v := reflect.ValueOf(s)
	field, _ := v.Type().FieldByName(fieldName)
	return field.Tag.Get("json")
}
