package main

import (
	"context"
	"flag"
	"fmt"

	"bitbucket.org/goldiesystems/tnzodapi/db"
	"bitbucket.org/goldiesystems/tnzodapi/internal/config"
	"bitbucket.org/goldiesystems/tnzodapi/internal/file"
	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
)

var cfg config.Config

func init() {
	// Load configuration
	var err error
	cfg, err = config.Load()
	if err != nil {
		fmt.Println("Error loading configuration file.", err)
		panic(err)
	}
}

func main() {
	fmt.Println("Compare Tourism New Zealand Data")

	// Parse flags
	version := flag.Int("v", 4, "Version of API to compare. (1 or 4)")
	flag.Parse()

	// Configure logging
	logFileT := fmt.Sprintf("%stnz-compare-%d-[T].txt", cfg.Logdir, *version)
	logFile, err := file.ReplaceFilenameWildcards(logFileT)
	if err != nil {
		fmt.Println("Unable to format log file", err)
		return
	}
	err = logger.InitAllFile(logFile)
	if err != nil {
		fmt.Println("Unable to initialise logger", err)
		return
	}
	if !cfg.Debug {
		logger.SuppressTrace()
	}

	// Configure database
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	err = db.Connect(ctx)
	if err != nil {
		fmt.Println("Unabled to connect to database", err)
		return
	}
	defer db.Disconnect()

	// Validate input
	if *version != 1 && *version != 4 {
		fmt.Println("Unknon value for API version -v. Use 1 or 4 only.")
		return
	}

	fmt.Printf("Comparing products from API v%d...\n", *version)
	logger.Info.Println("tnz-compare")
	logger.Info.Printf("version=%d\n", *version)

	// TODO: Change this comparison of v1 to v4 on "old" with comparing
	// equivalent versions between "old" and "new".
	p1, err := db.LoadProducts("old", 1) // TODO:!!!
	if err != nil {
		fmt.Println("Error loading products for old API v1", err)
		return
	}
	logger.Info.Printf("%d old products loaded\n", len(p1))
	p2, err := db.LoadProducts("old", 4) // TODO:!!!
	if err != nil {
		fmt.Println("Error loading products for old API v4", err)
		return
	}
	logger.Info.Printf("%d new products loaded\n", len(p2))

	// TODO: Actually compare
	comp, err := compare(p1, p2)
	if err != nil {
		fmt.Println("Error comparing products", err)
		return
	}

	// TODO: Actually store
	if err = db.ClearComparisons(*version); err != nil {
		logger.Error.Printf("Unable to clear existing comparison records from database")
		return
	}
	nfault := 0
	for _, c := range comp {
		if c.HasFault() {
			nfault++
			err = db.StoreComparison(c, *version) // TODO:!!!
			if err != nil {
				fmt.Println("Error storing comparison", err)
				return
			}
		}
	}
	logger.Trace.Printf("%d differences or missing listing IDs recorded\n", nfault)
}
