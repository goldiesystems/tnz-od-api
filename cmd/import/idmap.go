package main

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/goldiesystems/tnzodapi/db"
	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
	"bitbucket.org/goldiesystems/tnzodapi/model"
)

type objectInfo struct {
	ResolvedFrom core   `json:"resolved_from"`
	Type         string `json:"type"`
	OID          int    `json:"o_id"`
}

type core struct {
	Type string `json:"type"`
	OID  int    `json:"o_id"`
}

// mapProductIDs loops through all of the product listings in the product set
// and finds the listing ID associated with the core ID (for "old" APIs).
// The listing ID is stored on the RefID field if it is found, or 0 is stored
// if it cannot be determined and an error is recorded.
// It first searches the local cache for a mapping and calls out to an API if
// necessary, updating the local (file) cache.
func mapProductIDs(products []model.Product) error {
	m, err := db.LoadIDMap()
	if err != nil {
		return err
	}

	for p := range products {
		coreID := int(products[p].ODListingOID)
		// Check cache first
		listingID, ok := m[coreID]
		if !ok {
			// Get from API
			mapper, err := getMapperFromAPI(coreID)
			if err != nil {
				return err
			}
			if mapper.CoreID == 0 || mapper.ListingID == 0 {
				logger.Error.Printf("Unable to get valid map from API for coreID %d", coreID)
				listingID = 0
			} else {
				listingID = mapper.ListingID
			}
			// Update cache
			m[coreID] = listingID
			err = db.StoreIDMap(coreID, listingID)
			if err != nil {
				return err
			}
		}
		products[p].RefID = listingID
	}

	return nil
}

// getMapperFromAPI retrieves an IDMapper by reading the configured API end
// point. If the supplied ID is a "core" ID, you will get a full mapper,
// otherwise returns a nil mapper.
func getMapperFromAPI(id int) (model.IDMapper, error) {

	var mapper model.IDMapper

	api := cfg.GetIDMapEndpoint()
	url := fmt.Sprintf("%s?o_id=%d", api.BaseURL, id)
	logger.Trace.Print(url)

	// Prepare and send request
	req, err := prepareRequest(url, api.Token)
	if err != nil {
		return mapper, err
	}

	content, err := sendRequest(req, cfg.Delay, cfg.Timeout)
	if err != nil {
		return mapper, err
	}
	if len(content) > 0 {
		var data objectInfo
		err = json.Unmarshal(content, &data)
		if err != nil {
			logger.Error.Println(err)
			logger.Error.Println(string(content))
			return mapper, err
		}
		logger.Trace.Print(data)

		// Check and assemble mapper
		if data.ResolvedFrom != (core{}) {
			if data.ResolvedFrom.Type == "common.object.odlistingcore.Object" &&
				data.ResolvedFrom.OID != 0 &&
				data.Type == "common.object.odlisting.Object" &&
				data.OID != 0 {
				mapper = model.IDMapper{
					CoreID:    data.ResolvedFrom.OID,
					ListingID: data.OID,
				}
			}
		}
	}

	return mapper, nil
}

// Intended for use with "new" API where the listing OID is actually the ID
// and there is no concept of mapping to another "core" ID
func setProductIDs(products []model.Product) {
	for p := range products {
		products[p].RefID = int(products[p].ODListingOID)
	}
}
