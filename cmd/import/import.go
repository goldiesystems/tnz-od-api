package main

import (
	"fmt"

	"bitbucket.org/goldiesystems/tnzodapi/db"
	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
	"bitbucket.org/goldiesystems/tnzodapi/model"
)

func importProducts(environment string, version int, resume bool) error {

	var err error

	// Clear existing database records
	if !resume {
		if err = db.ClearProducts(environment, version); err != nil {
			fmt.Println("Error clearing existing products from database", err)
			return err
		}
		if err = db.ClearProductErrors(environment, version); err != nil {
			fmt.Println("Error clearing existing product errors from database", err)
			return err
		}
	}

	// Get/clear restore point
	var callSet []model.CallSet
	if resume {
		callSet, err = db.LoadCallSet(environment, version)
		if err != nil {
			fmt.Println("Error loading previous call set. Try running without resume.")
			return err
		}
	}
	if err = db.ClearCallSet(environment, version); err != nil {
		fmt.Println("Error clearing previous call set")
		return err
	}

	// For each region, for each market, page-by-page get the products up to
	// the configured limit
	totalretrieved := 0
	for _, r := range cfg.Regions {
		for _, m := range cfg.Markets {
			if resume && model.CallSetContains(callSet, model.CallSet{Region: r, Market: m}) {
				continue
			}
			page := 1
			retrieved := cfg.PageSize
			for retrieved >= cfg.PageSize {
				// Fetch
				products, proderrs, err := fetchProducts(environment, version, r, m, page)
				if err != nil {
					fmt.Println("Error retrieving product from API", err)
					return err
				}
				retrieved = len(products)
				totalretrieved += retrieved
				logger.Info.Printf("Total retrieved=%d\n", totalretrieved)

				// Store known data errors
				if len(proderrs) > 0 {
					logger.Info.Printf("%d known data errors were detected\n", len(proderrs))
					if err = db.StoreProductErrors(proderrs, environment, version); err != nil {
						fmt.Println("Error storing data error information", err)
						return err
					}
				}

				// Fix up Listing ID, Core ID thing
				if environment == "old" {
					if err = mapProductIDs(products); err != nil {
						fmt.Println("Error mapping product IDs", err)
						return err
					}
				} else {
					setProductIDs(products)
				}

				// Store
				if err = db.StoreProducts(products, environment, version); err != nil {
					fmt.Println("Error storing products", err)
					return err
				}

				page++
			}
			db.StoreCallSet(model.CallSet{Region: r, Market: m}, environment, version)
			if totalretrieved >= cfg.Limit {
				break
			}
		}
		if totalretrieved >= cfg.Limit {
			break
		}
	}

	return nil
}

func fetchProducts(
	environment string,
	version int,
	region string,
	market string,
	page int,
) ([]model.Product, []model.ProductError, error) {

	var products []model.Product
	var proderrs []model.ProductError
	tries := 1
	var err error

	for tries <= cfg.Retry {
		if version == 1 {
			products, proderrs, err = fetchV1Products(environment, region, market, page, tries)
		} else {
			products, proderrs, err = fetchV4Products(environment, region, market, page, tries)
		}
		if err == nil {
			break
		}
		logger.Error.Println("Retrying")
		tries++
	}

	return products, proderrs, err
}
