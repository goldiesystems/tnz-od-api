package main

import (
	"bytes"
	"encoding/json"
	"fmt"

	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
	"bitbucket.org/goldiesystems/tnzodapi/model"
)

// PageMeta4 is meta data associated with the page of product information
// returned from the v4 API
type PageMeta4 struct {
	Retrieved  int `json:"items_retrieved"`
	TotalItems int `json:"total_items"`
	PageSize   int `json:"max_items_to_retrieve"`
	Skipped    int `json:"items_skipped"`
}

// PageLink4 is paging link information associated with a page of product
// information returned from the v4 API
type PageLink4 struct {
	Last  string `json:"last"`
	Prev  string `json:"prev"`
	Next  string `json:"next"`
	First string `json:"first"`
}

// ProductPage4 is a page of product information returned from the v4 API
type ProductPage4 struct {
	Meta  PageMeta4       `json:"meta"`
	Link  PageLink4       `json:"link"`
	Items []model.Product `json:"items"`
}

func fetchV4Products(
	environment string,
	region string,
	market string,
	page int,
	tries int,
) ([]model.Product, []model.ProductError, error) {

	var products []model.Product
	var proderrs []model.ProductError

	v4 := cfg.GetProductEndpoint(environment, 4)

	skip := (page - 1) * cfg.PageSize
	url := fmt.Sprintf(
		"%s?%s&tag=%s&display_market=%s&skip=%d&maxrows=%d",
		v4.BaseURL,
		v4.BaseParams,
		region,
		market,
		skip,
		cfg.PageSize,
	)

	// Prepare and send request
	req, err := prepareRequest(url, v4.Token)
	if err != nil {
		return products, proderrs, err
	}
	content, err := sendRequest(req, tries*10*cfg.Delay, tries*cfg.Timeout)
	if err != nil {
		return products, proderrs, err
	}

	// Eliminate control characters from content (probably caused by copy-paste)
	content = bytes.ReplaceAll(content, []byte("\x1f"), []byte(" "))
	var data ProductPage4
	if err = json.Unmarshal(content, &data); err != nil {
		logger.Error.Println("Error unmarshalling content", err, string(content))
		return products, proderrs, err
	}

	retrieved := len(data.Items)
	logger.Trace.Printf("Retrieved=%d", retrieved)
	if retrieved > 0 {
		for _, v := range data.Items {
			if v.HasKnownError(market) {
				proderrs = append(proderrs, model.CreateProductError(v, url))
			} else {
				products = append(products, v)
			}
		}
	}
	return products, proderrs, nil
}
