package main

import (
	"context"
	"flag"
	"fmt"

	"bitbucket.org/goldiesystems/tnzodapi/db"
	"bitbucket.org/goldiesystems/tnzodapi/internal/config"
	"bitbucket.org/goldiesystems/tnzodapi/internal/file"
	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
)

var cfg config.Config

func init() {
	// Load configuration
	var err error
	cfg, err = config.Load()
	if err != nil {
		fmt.Println("Error loading configuration file.", err)
		panic(err)
	}
}

func main() {
	fmt.Println("Import Tourism New Zealand Data")

	// Parse flags
	environment := flag.String("e", "old", "Environment to import data from. ('old' or 'new')")
	version := flag.Int("v", 4, "Version of API to import from. (1 or 4)")
	resume := flag.Bool("resume", false, "Resume import from previous point")
	flag.Parse()

	// Configure logging
	logFileT := fmt.Sprintf("%stnz-import-%s-%d-[T].txt", cfg.Logdir, *environment, *version)
	logFile, err := file.ReplaceFilenameWildcards(logFileT)
	if err != nil {
		fmt.Println("Unable to format log file", err)
		return
	}
	err = logger.InitAllFile(logFile)
	if err != nil {
		fmt.Println("Unable to initialise logger", err)
		return
	}
	if !cfg.Debug {
		logger.SuppressTrace()
	}

	// Configure database
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	err = db.Connect(ctx)
	if err != nil {
		fmt.Println("Unabled to connect to database", err)
		return
	}
	defer db.Disconnect()

	// Validate input
	if *environment != "old" && *environment != "new" {
		fmt.Println("Unknown value for environment parameter -e. Use 'old' or 'new' only.")
		return
	}
	if *version != 1 && *version != 4 {
		fmt.Println("Unknown value for API version -v. Use 1 or 4 only.")
		return
	}

	fmt.Printf("Importing products from %s API v%d...\n", *environment, *version)
	logger.Info.Println("tnz-import")
	logger.Info.Printf("environment=%s\n", *environment)
	logger.Info.Printf("version=%d\n", *version)
	if *resume {
		fmt.Println("Resuming from previous run")
		logger.Info.Println("Resuming from previous run")
	}

	if err = importProducts(*environment, *version, *resume); err != nil {
		fmt.Println("Error importing products from API")
	}
}
