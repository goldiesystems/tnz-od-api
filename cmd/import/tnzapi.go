package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
)

func prepareRequest(url string, token string) (*http.Request, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		logger.Error.Print(err)
		return req, err
	}
	req.Header.Set("User-Agent", "goldie-api-tester")
	req.Header.Set("Accept", "application/json")
	if token != "" {
		auth := fmt.Sprintf("Bearer %s", token)
		req.Header.Set("Authorization", auth)
	}
	logger.Trace.Printf("Request.URL=%s", url)
	return req, nil
}

func sendRequest(req *http.Request, delay int, timeout int) ([]byte, error) {
	var content []byte

	client := http.Client{
		Timeout: time.Duration(timeout) * time.Second,
	}
	time.Sleep(time.Duration(delay) * time.Millisecond)
	res, err := client.Do(req)
	if err != nil {
		logger.Error.Print(err)
		return content, err
	}
	logger.Trace.Printf("Response.Status=%s", res.Status)
	if len(res.Status) >= 3 && res.Status[:3] == "200" {
		content, err = ioutil.ReadAll(res.Body)
		if err != nil {
			logger.Error.Print(err)
		}
	}
	return content, err
}
