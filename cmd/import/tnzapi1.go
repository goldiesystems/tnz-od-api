package main

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"

	"bitbucket.org/goldiesystems/tnzodapi/internal/logger"
	"bitbucket.org/goldiesystems/tnzodapi/model"
)

// Error1 is a flag indicating whether or not an error occurred in the request
type Error1 struct {
	Occurred bool `json:"occurred"`
}

// ProductPage1 is a page of product information returned from the v1 API
type ProductPage1 struct {
	Errors        []map[string]interface{} `json:"errors"`
	ExecutionTime int                      `json:"executiontime"`
	Error         Error1                   `json:"error"`
	Value         []model.Product          `json:"value"`
	MetaData      map[string]interface{}   `json:"metadata"`
}

func fetchV1Products(
	environment string,
	region string,
	market string,
	page int,
	tries int,
) ([]model.Product, []model.ProductError, error) {

	var products []model.Product
	var proderrs []model.ProductError

	v1 := cfg.GetProductEndpoint(environment, 1)
	startindex := (page-1)*cfg.PageSize + 1
	qs := createQueryString(
		v1.BaseParams,
		region,
		market,
		startindex,
		cfg.PageSize,
		v1.Token,
	)
	url := fmt.Sprintf("%s?%s", v1.BaseURL, qs)

	// Prepare and send request
	req, err := prepareRequest(url, "")
	if err != nil {
		return products, proderrs, err
	}
	content, err := sendRequest(req, tries*10*cfg.Delay, tries*cfg.Timeout)
	if err != nil {
		return products, proderrs, err
	}

	// Eliminate control characters from content (probably caused by copy-paste)
	content = bytes.ReplaceAll(content, []byte("\x1f"), []byte(" "))
	var data ProductPage1
	if err = json.Unmarshal(content, &data); err != nil {
		logger.Error.Println("Unable to unmarshal content", err, string(content))
		return products, proderrs, err
	}

	// Check for errors
	if data.Error.Occurred {
		logger.Error.Printf("Error returned from v1 API %v", data.Errors)
		return products, proderrs, errors.New("Error returned from v1 API")
	}

	// Check for any values returned and accumulate.
	retrieved := len(data.Value)
	logger.Trace.Printf("Retrieved=%d", retrieved)
	if retrieved > 0 {
		for _, v := range data.Value {
			if v.HasKnownError(market) {
				proderrs = append(proderrs, model.CreateProductError(v, url))
			}
			products = append(products, v)
		}
	}
	return products, proderrs, nil
}

func createQueryString(
	baseParams string,
	region string,
	market string,
	startindex int,
	maximum int,
	secret string,
) string {
	qs := fmt.Sprintf(
		"%s&tag=%s&market=%s&startindex=%d&maximum=%d",
		baseParams,
		region,
		market,
		startindex,
		maximum,
	)
	hasher := md5.New()
	hasher.Write([]byte(qs + secret))
	signature := hex.EncodeToString(hasher.Sum(nil))
	return fmt.Sprintf("%s&signature=%s", qs, signature)
}
