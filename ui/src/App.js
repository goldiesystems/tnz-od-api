import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import Header from './components/Header';
import Routes from './Routes';

import customTheme from './customTheme'

const theme = createMuiTheme(customTheme);

const App = props => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BrowserRouter>
        <header>
          <Header />
        </header>
        <main>
          <Container maxWidth="lg" style={{ marginTop: theme.spacing(2) }}>
            <Typography variant="subtitle1" component="h1">Tourism New Zealand</Typography>
            <Typography variant="body2">Business Database API Comparison</Typography>
            <Routes />
          </Container>
        </main>
      </BrowserRouter>
    </ThemeProvider>
  );
};

export default App;
