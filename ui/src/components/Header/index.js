import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import HomeIcon from '@material-ui/icons/Home';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const styles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    toolbarIcon: {
        color: theme.palette.primary.contrastText,
    },
    title: {
        flexGrow: 1,
    },
}));

const Header = props => {

    const classes = styles();

    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton}>
                    <RouterLink to="/"><HomeIcon className={classes.toolbarIcon} /></RouterLink>
                </IconButton>
                <Typography variant="h6" component="h1" className={classes.title}>
                    API Comparer
                </Typography>
            </Toolbar>
        </AppBar>
    );
};

Header.propTypes = {
}

export default Header;