import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/styles';

const styles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
    },
    summaryCounts: {
        marginTop: theme.spacing(1),
    },
    header: {
        fontWeight: 'bold',
    },
    number: {
        textAlign: 'right',
        minWidth: theme.spacing(8),
    },
    diffs: {
        marginTop: theme.spacing(2),
    },
}));

const SummaryItem = props => {

    const classes = styles();

    const [redirect, setRedirect] = useState("");

    const onDiffClick = event => {
        setRedirect("/comparison/" + props.version);
    };

    const summary = (
        <Paper className={classes.paper}>
            <Typography variant="h6">
                {props.version === 1 ? "Legacy" : "REST"}
            </Typography>
            <Divider />
            <Grid container spacing={2} justify="space-evenly" className={classes.summaryCounts}>
                <Grid item>
                    <Grid container>
                        <Grid item>
                            <Grid container>
                                <Grid item className={classes.header}>Old:</Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container direction="column" spacing={1}>
                                <Grid item>
                                    <Grid container spacing={1}>
                                        <Grid item className={classes.number}>{props.summary.old.count}</Grid>
                                        <Grid item>products</Grid>
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Grid container spacing={1}>
                                        <Grid item className={classes.number}>{props.summary.old.errors}</Grid>
                                        <Grid item>errors</Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item>
                    <Grid container>
                        <Grid item>
                            <Grid container>
                                <Grid item className={classes.header}>New:</Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container direction="column" spacing={1}>
                                <Grid item>
                                    <Grid container spacing={1}>
                                        <Grid item className={classes.number}>{props.summary.new.count}</Grid>
                                        <Grid item>products</Grid>
                                    </Grid>
                                </Grid>
                                {props.summary.new.errors > 0 &&
                                    <Grid item>
                                        <Grid container spacing={1}>
                                            <Grid item className={classes.number}>{props.summary.new.errors}</Grid>
                                            <Grid item>errors</Grid>
                                        </Grid>
                                    </Grid>
                                }
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container justify="center" className={classes.diffs}>
                <Grid item>
                    <Button onClick={onDiffClick} color="secondary">{props.summary.diffs} Differences</Button>
                </Grid>
            </Grid>
        </Paper>
    );

    return (
        <div>
            {(redirect === "") ? summary : <Redirect to={redirect} />}
        </div>
    );
};

SummaryItem.propTypes = {
    version: PropTypes.number.isRequired,
    summary: PropTypes.shape({
        old: PropTypes.shape({
            count: PropTypes.number.isRequired,
            errors: PropTypes.number.isRequired,
        }).isRequired,
        new: PropTypes.shape({
            count: PropTypes.number.isRequired,
            errors: PropTypes.number.isRequired,
        }).isRequired,
        diffs: PropTypes.number.isRequired,
    }).isRequired,
};

export default SummaryItem;
