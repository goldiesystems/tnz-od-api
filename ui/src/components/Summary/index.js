import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/styles';
import { connect } from 'react-redux';

import ErrorMessage from '../UI/ErrorMessage';
import Loader from '../UI/Loader';
import SummaryItem from './SummaryItem';
import { getSummary, clearSummary } from '../../store/actions/summary';

const styles = makeStyles(theme => ({
    gridContainer: {
        marginTop: theme.spacing(2),
    },
}));

const Summary = props => {
    const { onLoad, onClear } = props;
    const classes = styles();

    useEffect(() => {
        onLoad();
    }, [onLoad]);

    const handleReload = () => {
        onClear();
        onLoad();
    }

    return (
        props.summary.isLoading
            ? <Loader />
            : (
                <Fragment>
                    <Grid container spacing={3} className={classes.gridContainer}>
                        <Grid item xs={6}>
                            <SummaryItem
                                version={1}
                                summary={props.summary.v1}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <SummaryItem
                                version={4}
                                summary={props.summary.v4}
                            />
                        </Grid>
                    </Grid>
                    <ErrorMessage error={props.summary.error} />
                    <Button onClick={handleReload}>Reload</Button>
                </Fragment>
            )
    );
};

Summary.propTypes = {
    summary: PropTypes.object,
    onLoad: PropTypes.func.isRequired,
    onClear: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
    return {
        summary: state.summary,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onLoad: () => dispatch(getSummary()),
        onClear: () => dispatch(clearSummary()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Summary);