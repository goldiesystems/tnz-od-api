import React, { Fragment } from 'react';
import ErrorIcon from '@material-ui/icons/Error'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';

import { productSummaryPropType } from './propTypes';

const styles = makeStyles(theme => ({
    thumbnail: {
        maxWidth: 260,
    },
    idName: {
        minWidth: 80,
    },
    error: {
        color: theme.palette.error.main,
        fontWeight: 600,
    },
}));

const ProductSummary = props => {

    const classes = styles();

    const productSummary = (
        <Grid container spacing={2}>
            <Grid item>
                <img
                    src={props.prod.thumbnail}
                    alt="thumbnail"
                    className={classes.thumbnail}
                />
            </Grid>
            <Grid item>
                <Grid container direction="column">
                    <Grid item>
                        <Typography variant="subtitle1">{props.prod.name} ({props.prod.market})</Typography>
                    </Grid>
                    <Grid item>
                        <Grid container spacing={1}>
                            <Grid item className={classes.idName}>
                                <Typography variant="body2" className={props.prod.listingID === 0 ? classes.error : null}>Listing OID:</Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="body2" className={props.prod.listingID === 0 ? classes.error : null}>{props.prod.listingID}</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Grid container spacing={1}>
                            <Grid item className={classes.idName}>
                                <Typography variant="body2">Core OID:</Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="body2">{props.prod.coreID}</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    {props.children}
                </Grid>
            </Grid>
        </Grid>
    );

    const missingProduct = (
        <Grid container spacing={1}>
            <Grid item>
                <ErrorIcon />
            </Grid>
            <Grid item>
                <Typography variant="body2">Product missing</Typography>
            </Grid>
        </Grid>
    )

    return (
        <Fragment>
            {props.prod.coreID === 0 && props.prod.listingID === 0
                ? missingProduct
                : productSummary}
        </Fragment>
    )
};

ProductSummary.propTypes = {
    prod: productSummaryPropType.isRequired,
};

export default ProductSummary;
