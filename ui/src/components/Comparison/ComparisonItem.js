import React, { Fragment } from 'react';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';

import ProductSummary from './ProductSummary';
import DiffList from './DiffList';
import { comparisonPropType } from './propTypes';

const ComparisonItem = props => {
    return (
        <Fragment>
            <ListItem>
                <Grid container spacing={1}>
                    <Grid item xs={6}>
                        <ProductSummary prod={props.comparisonItem.old}>
                            <DiffList env="old" diff={props.comparisonItem.diff} />
                        </ProductSummary>
                    </Grid>
                    <Grid item xs={6}>
                        <ProductSummary prod={props.comparisonItem.new}>
                            <DiffList env="new" diff={props.comparisonItem.diff} />
                        </ProductSummary>
                    </Grid>
                </Grid>
            </ListItem>
            <Divider />
        </Fragment>
    );
};

ComparisonItem.propTypes = {
    comparisonItem: comparisonPropType.isRequired,
};

export default ComparisonItem;