import PropTypes from 'prop-types';

export const productSummaryPropType = PropTypes.shape({
    listingID: PropTypes.number,
    coreID: PropTypes.number,
    market: PropTypes.string,
    name: PropTypes.string,
    thumbnail: PropTypes.string,
    message: PropTypes.string,
});

export const differencePropType = PropTypes.shape({
    fieldTag: PropTypes.string.isRequired,
    oldValue: PropTypes.string.isRequired,
    newValue: PropTypes.string.isRequired,
});

export const comparisonPropType = PropTypes.shape({
    key: PropTypes.string.isRequired,
    old: productSummaryPropType.isRequired,
    new: productSummaryPropType.isRequired,
    diff: PropTypes.arrayOf(differencePropType),
});

export const comparisonPagePropType = PropTypes.shape({
    version: PropTypes.number.isRequired,
    skip: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired,
    records: PropTypes.number.isRequired,
    count: PropTypes.number.isRequired,
    message: PropTypes.string.isRequired,
    data: PropTypes.arrayOf.comparisonPropType,
});

