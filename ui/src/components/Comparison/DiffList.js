import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';

import { differencePropType } from './propTypes';

const styles = makeStyles(theme => ({
    idName: {
        minWidth: 150,
    },
}));


const DiffList = props => {

    const classes = styles();

    const listItems = () => {
        if (props.diff && props.diff.length > 0) {
            return props.diff.map((d) => (
                <Grid item key={props.env + "_" + d.fieldTag}>
                    <Grid container>
                        <Grid item className={classes.idName}>
                            <Typography variant="body2" color="secondary">
                                {d.fieldTag}:
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2" color="secondary">
                                {props.env === "old" ? d.oldValue : d.newValue}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            ));
        }
        return null;
    }

    return (
        <div>
            {listItems()}
        </div>
    )
}

DiffList.propTypes = {
    env: PropTypes.string.isRequired,
    diff: PropTypes.arrayOf(differencePropType),
};

export default DiffList;