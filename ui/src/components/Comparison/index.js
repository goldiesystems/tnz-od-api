import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';
import { connect } from 'react-redux';

import ErrorMessage from '../UI/ErrorMessage';
import Loader from '../UI/Loader';
import ComparisonItem from './ComparisonItem';
import Pager from '../Pager';
import { comparisonPagePropType } from './propTypes';
import config from '../../config';
import { getCurrentComparisonPage } from '../../store/selectors';
import { getComparison, clearComparison } from '../../store/actions/comparison';

const styles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
    },
    columnTitle: {
        textAlign: 'center',
    },
}));

const Comparison = props => {
    const classes = styles();
    let version = props.version;
    if (props.match.params && props.match.params.version) {
        version = parseInt(props.match.params.version, 10);
    }
    const { onLoad, onClear, skip } = props;

    useEffect(() => {
        onLoad(version, skip);
    }, [onLoad, version, skip]);

    const handlePageChange = (newSkip) => {
        onLoad(version, newSkip);
    }

    const handleReload = () => {
        onClear();
        onLoad(version, skip);
    }

    const comparisonItems = () => {
        if (props.comps && props.comps.data && props.comps.data.length > 0) {
            return (props.comps.data.map((comp) =>
                <ComparisonItem key={comp.key} comparisonItem={comp} />
            ));
        }
        return null;
    };

    const title = (
        <Typography variant="h5" component="h2" color="secondary">
            {version === 1 ? "Legacy" : "REST"} API
        </Typography>
    );

    return (
        props.isLoading
            ? <Loader />
            : (
                <Fragment>
                    <Grid container direction="column" alignItems="center">
                        <Grid item xs={12}>
                            {title}
                        </Grid>
                    </Grid>
                    <Pager skip={skip} limit={config.API.pageSize} max={props.comps == null ? 0 : props.comps.count} onPageChange={handlePageChange} />
                    <Paper className={classes.root}>
                        <Grid container>
                            <Grid item xs={6} className={classes.columnTitle}>
                                <Typography variant="subtitle1">OLD</Typography>
                            </Grid>
                            <Grid item xs={6} className={classes.columnTitle}>
                                <Typography variant="subtitle1">NEW</Typography>
                            </Grid>
                        </Grid>
                        <Divider />
                        <List>
                            {comparisonItems()}
                        </List>
                    </Paper>
                    <Pager skip={skip} limit={config.API.pageSize} max={props.comps == null ? 0 : props.comps.count} onPageChange={handlePageChange} />
                    <ErrorMessage error={props.error} />
                    <Button onClick={handleReload}>Reload</Button>
                </Fragment>
            )
    );
};

Comparison.propTypes = {
    error: PropTypes.object,
    isLoading: PropTypes.bool.isRequired,
    version: PropTypes.number.isRequired,
    skip: PropTypes.number.isRequired,
    comps: comparisonPagePropType,
};

const mapStateToProps = state => {
    return {
        error: state.comparison.error,
        isLoading: state.comparison.isLoading,
        version: state.comparison.currentVersion,
        skip: state.comparison.currentSkip,
        comps: getCurrentComparisonPage(state),
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLoad: (version, skip) => dispatch(getComparison(version, skip)),
        onClear: () => dispatch(clearComparison()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Comparison);
