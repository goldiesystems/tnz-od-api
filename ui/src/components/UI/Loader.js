import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        paddingTop: theme.spacing(3),
    },
});

const Loader = props => {
    const { classes } = props;

    return (
        <div className={classes.root}>
            <CircularProgress color="primary" style={{ color: '#000000' }} />
        </div>
    );
};

export default withStyles(styles)(Loader);
