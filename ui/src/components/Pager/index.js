import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import LastPageIcon from '@material-ui/icons/LastPage';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/styles';

const styles = makeStyles(theme => ({
    root: {
        textAlign: 'right',
    },
}));

const Pager = props => {

    const classes = styles();
    const currentPage = Math.floor(props.skip / props.limit) + 1;
    const endPage = Math.floor(props.max / props.limit) + 1;

    const onGo = page => {
        if (page > 0 && page <= endPage) {
            const skip = (page - 1) * props.limit;
            props.onPageChange(skip);
        }
    };

    return (
        <Grid container spacing={1}>
            <Grid item xs={12} className={classes.root}>
                {currentPage > 1 && (
                    <Fragment>
                        <IconButton onClick={() => onGo(1)}>
                            <FirstPageIcon />
                        </IconButton>
                        <IconButton onClick={() => onGo(currentPage - 1)}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </Fragment>
                )}
                {currentPage} / {endPage}
                {currentPage < endPage && (
                    <Fragment>
                        <IconButton onClick={() => onGo(currentPage + 1)}>
                            <ChevronRightIcon />
                        </IconButton>
                        <IconButton onClick={() => onGo(endPage)}>
                            <LastPageIcon />
                        </IconButton>
                    </Fragment>
                )}
            </Grid>
        </Grid>
    );
};

Pager.propTypes = {
    skip: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
};

export default Pager;
