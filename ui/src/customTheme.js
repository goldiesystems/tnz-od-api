const HEADING_FONT = ['Roboto', 'sans-serif'].join(',');
const BODY_FONT = ['Roboto', 'sans-serif'].join(',');
export const PRIMARY_LIGHT_COLOR = '#62727b';
export const PRIMARY_MAIN_COLOR = '#37474f';
export const PRIMARY_DARK_COLOR = '#102027';
export const ON_PRIMARY_COLOR = '#ffffff';
export const SECONDARY_LIGHT_COLOR = '#f05545';
export const SECONDARY_MAIN_COLOR = '#b71c1c';
export const SECONDARY_DARK_COLOR = '#7f0000';
export const ON_SECONDARY_COLOR = '#ffffff';
export const BACKGROUND_COLOR = '#fafafa';
export const PAPER_COLOR = '#ffffff';

export default {
    palette: {
        primary: {
            light: PRIMARY_LIGHT_COLOR,
            main: PRIMARY_MAIN_COLOR,
            dark: PRIMARY_DARK_COLOR,
            contrastText: ON_PRIMARY_COLOR
        },
        secondary: {
            light: SECONDARY_LIGHT_COLOR,
            main: SECONDARY_MAIN_COLOR,
            dark: SECONDARY_DARK_COLOR,
            contrastText: ON_SECONDARY_COLOR,
        },
        background: {
            default: BACKGROUND_COLOR,
            paper: PAPER_COLOR,
        },
    },
    typography: {
        fontFamily: BODY_FONT,
        h1: {
            fontFamily: HEADING_FONT,
            fontWeight: 500
        },
        h2: {
            fontFamily: HEADING_FONT,
            fontWeight: 400
        },
        h3: {
            fontFamily: HEADING_FONT,
            fontWeight: 500
        },
        h4: {
            fontFamily: HEADING_FONT,
            fontWeight: 400
        },
        h5: {
            fontFamily: HEADING_FONT,
            fontWeight: 500
        },
        h6: {
            fontFamily: HEADING_FONT,
            fontWeight: 500
        },
        subtitle1: {
            fontFamily: HEADING_FONT,
            fontWeight: 500
        },
        button: {
            fontWeight: 600
        },
    },
}