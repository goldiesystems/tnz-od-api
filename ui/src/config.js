const dev = {
    API: {
        getSummaryURL: "http://localhost/summary",
        getComparisonURL: "http://localhost/comparisons",
        pageSize: 30,
    },
};

const test = dev;

const prod = {
    API: {
        getSummaryURL: "https://tnzapi.goldiesystems.com/summary",
        getComparisonURL: "https://tnzapi.goldiesystems.com/comparisons",
        pageSize: 30,
    },
};

var config;

switch (process.env.NODE_ENV) {
    case 'production':
        config = prod;
        break;
    case 'test':
        config = test;
        break;
    default:
        config = dev;
};

export default config;
