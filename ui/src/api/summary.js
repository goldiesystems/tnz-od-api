import config from '../config';

export const fetchSummary = async () => {
    const req = {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const res = await fetch(config.API.getSummaryURL, req);
    if (!res.ok) {
        throw new Error(res.status + ':' + res.statusText);
    }
    return await res.json();
}