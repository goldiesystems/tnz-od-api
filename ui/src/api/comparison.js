import config from '../config';

export const fetchComparison = async (version, skip) => {
    const req = {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const params =
        "?version=" + version
        + "&skip=" + skip
        + "&limit=" + config.API.pageSize;

    console.log("Fetching comparison with params ", params);

    const res = await fetch(config.API.getComparisonURL + params, req);
    if (!res.ok) {
        throw new Error(res.status + ":" + res.statusText);
    }
    return await res.json();
}