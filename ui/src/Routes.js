import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import Loader from './components/UI/Loader';

const Summary = React.lazy(() => import('./components/Summary'));
const Comparison = React.lazy(() => import('./components/Comparison'));

const Routes = props => {
    return (
        <Suspense fallback={<Loader />}>
            <Switch>
                <Route path="/" exact component={Summary} />
                <Route path="/comparison/:version(1|4)" exact component={Comparison} />
                <Redirect to="/" />
            </Switch>
        </Suspense>
    );
};

export default Routes;