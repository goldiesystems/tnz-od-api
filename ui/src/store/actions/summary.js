import * as actionTypes from './types';

export const getSummary = () => {
    return {
        type: actionTypes.GET_SUMMARY,
    };
};

export const getSummaryInit = () => {
    return {
        type: actionTypes.GET_SUMMARY_INIT,
    };
};

export const getSummarySuccess = summary => {
    return {
        type: actionTypes.GET_SUMMARY_SUCCESS,
        payload: {
            summary,
        },
    };
};

export const getSummaryError = error => {
    return {
        type: actionTypes.GET_SUMMARY_ERROR,
        payload: {
            error,
        },
    };
};

export const clearSummary = () => {
    return {
        type: actionTypes.CLEAR_SUMMARY,
    };
};
