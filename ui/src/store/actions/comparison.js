import * as actionTypes from './types';

export const getComparison = (version, skip) => {
    return {
        type: actionTypes.GET_COMPARISON,
        payload: {
            version,
            skip,
        },
    };
};

export const getComparisonInit = () => {
    return {
        type: actionTypes.GET_COMPARISON_INIT,
    };
};

export const getComparisonSuccess = (comps) => {
    return {
        type: actionTypes.GET_COMPARISON_SUCCESS,
        payload: {
            comps,
        },
    };
};

export const getComparisonError = error => {
    return {
        type: actionTypes.GET_COMPARISON_ERROR,
        payload: {
            error,
        },
    };
};

export const clearComparison = () => {
    return {
        type: actionTypes.CLEAR_COMPARISON,
    };
};

export const setCurrentPage = (version, skip) => {
    return {
        type: actionTypes.SET_COMPARISON_PAGE,
        payload: {
            version,
            skip,
        },
    };
};

