import { combineReducers } from 'redux';
import summaryReducer from './summary';
import comparisonReducer from './comparison';

const rootReducer = combineReducers({
    summary: summaryReducer,
    comparison: comparisonReducer,
});

export default rootReducer;
