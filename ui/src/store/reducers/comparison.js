import * as actionTypes from '../actions/types';

// For documentation purposes
// const blankComparison = {
//     key: "",
//     old: {
//         listingID: 0,
//         coreID: 0,
//         market: "",
//         name: "",
//         thumbnail: "",
//         message: ""
//     },
//     new: {
//         listingID: 0,
//         coreID: 0,
//         market: "",
//         name: "",
//         thumbnail: "",
//         message: ""
//     },
//     diff: [
//         {
//             fieldTag: "",
//             oldValue: "",
//             newValue: "",
//         },
//     ],
// }

// For documentation purposes. (Data is an array of comparisons).
// const blankPageMeta = {
//     version: 4,
//     skip: 0,
//     limit: config.API.pageSize,
//     records: 0,
//     count: 0,
//     message: "",
//     data: [],
// }

// Comparisons are stored as "pages" of arrays with some meta data
export const initialState = {
    pages: [],
    currentSkip: 0,
    currentVersion: 4,
    error: null,
    isLoading: false,
};

// Returns a copy of the current state but with error reset and isLoading true.
const getComparisonInit = (state) => {
    return {
        ...state,
        pages: state.pages.map((page, i) => {
            return {
                ...page,
                data: page.data.map((comp, j) => {
                    return {
                        ...comp,
                        old: {
                            ...comp.old,
                        },
                        new: {
                            ...comp.new,
                        },
                        diff: comp.diff.map((d, k) => {
                            return {
                                ...d
                            };
                        }),
                    };
                }),
            };
        }),
        error: null,
        isLoading: true,
    };
};

// Adds a page of comparisons to the state, resets error and sets
// isLoading to false. Note: will add page regardless of whether an
// equivalent one already exists, so check needs to be done outside
// this function if duplicates are undesireable.
const getComparisonSuccess = (state, payload) => {
    const { comps } = payload;
    if (comps.data == null) {
        return state;
    }

    const newPage = {
        ...comps,
        data: comps.data.map((comp, j) => {
            return {
                ...comp,
                old: {
                    ...comp.old,
                },
                new: {
                    ...comp.new,
                },
                diff: comp.diff == null ? [] : comp.diff.map((d, k) => {
                    return {
                        ...d
                    };
                }),
            };
        }),
    };

    let newState = {
        ...state,
        pages: state.pages.map((page, i) => {
            return {
                ...page,
                data: page.data.map((comp, j) => {
                    return {
                        ...comp,
                        old: {
                            ...comp.old,
                        },
                        new: {
                            ...comp.new,
                        },
                        diff: comp.diff.map((d, k) => {
                            return {
                                ...d
                            };
                        }),
                    };
                }),
            };
        }),
        currentVersion: newPage.version,
        currentSkip: newPage.skip,
        error: null,
        isLoading: false,
    };
    newState.pages.push(newPage);
    return newState;
};

// Returns a copy of the given state with isLoading reset and error object set
const getComparisonError = (state, payload) => {
    return {
        ...state,
        pages: state.pages.map((page, i) => {
            return {
                ...page,
                data: page.data.map((comp, j) => {
                    return {
                        ...comp,
                        old: {
                            ...comp.old,
                        },
                        new: {
                            ...comp.new,
                        },
                        diff: comp.diff.map((d, k) => {
                            return {
                                ...d
                            };
                        }),
                    };
                }),
            };
        }),
        error: payload.error,
        isLoading: false,
    };
};

const setComparisonPage = (state, payload) => {
    const { version, skip } = payload;
    return {
        ...state,
        pages: state.pages.map((page, i) => {
            return {
                ...page,
                data: page.data.map((comp, j) => {
                    return {
                        ...comp,
                        old: {
                            ...comp.old,
                        },
                        new: {
                            ...comp.new,
                        },
                        diff: comp.diff.map((d, k) => {
                            return {
                                ...d
                            };
                        }),
                    };
                }),
            };
        }),
        currentVersion: version,
        currentSkip: skip,
    };
}

const clearComparison = () => {
    return {
        pages: [],
        currentVersion: 4,
        currentSkip: 0,
        error: null,
        isLoading: false,
    };
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_COMPARISON_INIT:
            return getComparisonInit(state);
        case actionTypes.GET_COMPARISON_SUCCESS:
            return getComparisonSuccess(state, action.payload);
        case actionTypes.GET_COMPARISON_ERROR:
            return getComparisonError(state, action.payload);
        case actionTypes.CLEAR_COMPARISON:
            return clearComparison();
        case actionTypes.SET_COMPARISON_PAGE:
            return setComparisonPage(state, action.payload);
        default:
            return state;
    };
};

export default reducer;