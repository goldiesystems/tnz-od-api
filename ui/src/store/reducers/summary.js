import * as actionTypes from '../actions/types';

export const initialState = {
    v1: {
        old: {
            count: 0,
            errors: 0,
        },
        new: {
            count: 0,
            errors: 0,
        },
        diffs: 0,
    },
    v4: {
        old: {
            count: 0,
            errors: 0,
        },
        new: {
            count: 0,
            errors: 0,
        },
        diffs: 0,
    },
    error: null,
    isLoading: false,
    hasLoaded: false,
};

const getSummaryInit = state => {
    return {
        ...state,
        v1: {
            ...state.v1,
            old: {
                ...state.v1.old,
            },
            new: {
                ...state.v1.new,
            },
        },
        v4: {
            ...state.v4,
            old: {
                ...state.v4.old,
            },
            new: {
                ...state.v4.new,
            },
        },
        error: null,
        isLoading: true,
    };
};

const getSummarySuccess = (state, payload) => {
    const { summary } = payload;
    return {
        ...state,
        v1: {
            ...state.v1,
            old: {
                ...state.v1.old,
                count: summary.v1.old.count,
                errors: summary.v1.old.errors,
            },
            new: {
                ...state.v1.new,
                count: summary.v1.new.count,
                errors: summary.v1.new.errors,
            },
            diffs: summary.v1.diffs,
        },
        v4: {
            ...state.v4,
            old: {
                ...state.v4.old,
                count: summary.v4.old.count,
                errors: summary.v4.old.errors,
            },
            new: {
                ...state.v4.new,
                count: summary.v4.new.count,
                errors: summary.v4.new.errors,
            },
            diffs: summary.v4.diffs,
        },
        error: null,
        isLoading: false,
        hasLoaded: true,
    }
}

const getSummaryError = (state, payload) => {
    return {
        ...state,
        v1: {
            ...state.v1,
            old: {
                ...state.v1.old,
            },
            new: {
                ...state.v1.new,
            },

        },
        v4: {
            ...state.v4,
            old: {
                ...state.v4.old,
            },
            new: {
                ...state.v4.new,
            },
        },
        error: payload.error,
        isLoading: false,
        hasLoaded: false,
    };
}

const clearSummary = () => {
    return {
        ...initialState,
        v1: {
            ...initialState.v1,
            old: {
                ...initialState.v1.old,
            },
            new: {
                ...initialState.v1.new,
            },
        },
        v4: {
            ...initialState.v4,
            old: {
                ...initialState.v4.old,
            },
            new: {
                ...initialState.v4.new,
            },
        },
    };
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_SUMMARY_INIT:
            return getSummaryInit(state);
        case actionTypes.GET_SUMMARY_SUCCESS:
            return getSummarySuccess(state, action.payload);
        case actionTypes.GET_SUMMARY_ERROR:
            return getSummaryError(state, action.payload);
        case actionTypes.CLEAR_SUMMARY:
            return clearSummary();
        default:
            return state;
    };
};

export default reducer;