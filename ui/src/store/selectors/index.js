
export const hasSummaryLoaded = state => state.summary.hasLoaded;

export const hasComparisonPageLoaded = (state, version, skip) => {
    const { comparison } = state;

    console.log("Checking page loaded for version, skip ", version, skip);
    console.log("Number of pages loaded ", comparison.pages.length);

    for (let i = 0; i < comparison.pages.length; i++) {
        const p = comparison.pages[i];
        console.log("Page", i, " version, skip ", p.version, p.skip);
        console.log("Compare with version, skip", version, skip);
        if (p.version === version && p.skip === skip) {
            console.log("Loaded already");
            return true;
        }
    };
    return false;
};

export const getCurrentComparisonPage = (state) => {
    const { comparison } = state;

    for (let i = 0; i < comparison.pages.length; i++) {
        const p = comparison.pages[i];
        if (p.version === comparison.currentVersion && p.skip === comparison.currentSkip) {
            return p;
        }
    };
    return null;
};
