import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers';
import { initialState as summaryInitialState } from './reducers/summary';
import rootSaga from './sagas';

export const initialState = {
    summary: summaryInitialState,
};

export const configureStore = () => {
    // Chrome redux devtools extension
    const composeEnhancers = (
        process.env.NODE_ENV === 'development'
            ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
            : null
    ) || compose;

    const sagaMiddleware = createSagaMiddleware();
    const store = {
        ...createStore(
            rootReducer,
            initialState,
            composeEnhancers(applyMiddleware(sagaMiddleware))
        ),
        runSaga: sagaMiddleware.run(rootSaga),
    };

    return store;
};