import { call, put, select } from 'redux-saga/effects';

import * as summaryActions from '../actions/summary';
import { hasSummaryLoaded } from '../selectors';
import { fetchSummary } from '../../api/summary';

export function* getSummarySaga(action) {
    try {
        const checkLoaded = yield select(hasSummaryLoaded);
        if (!checkLoaded) {
            const summary = yield call(fetchSummary);
            yield put(summaryActions.getSummarySuccess(summary));
        }
    } catch (e) {
        yield put(summaryActions.getSummaryError(e));
    }
}
