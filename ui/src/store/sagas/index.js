import { all, fork, takeEvery } from 'redux-saga/effects';

import * as actionTypes from '../actions/types';

import { getSummarySaga } from './summary';
import { getComparisonSaga } from './comparison';

function* watchSummary() {
    yield all([
        yield takeEvery(actionTypes.GET_SUMMARY, getSummarySaga),
    ]);
}

function* watchComparison() {
    yield all([
        yield takeEvery(actionTypes.GET_COMPARISON, getComparisonSaga),
    ]);
}

export default function* rootSaga() {
    yield all([
        yield fork(watchSummary),
        yield fork(watchComparison),
    ]);
};
