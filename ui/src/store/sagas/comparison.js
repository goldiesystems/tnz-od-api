import { call, put, select } from 'redux-saga/effects';

import * as comparisonActions from '../actions/comparison';
import { hasComparisonPageLoaded } from '../selectors';
import { fetchComparison } from '../../api/comparison';

export function* getComparisonSaga(action) {
    const { version, skip } = action.payload;
    console.log("getComparisonSaga called with version, skip", version, skip);
    try {
        const checkLoaded = yield select(hasComparisonPageLoaded, version, skip);
        if (!checkLoaded) {
            const comps = yield call(fetchComparison, version, skip);
            console.log("Received comparisons from API: ", comps);
            yield put(comparisonActions.getComparisonSuccess(comps));
        } else {
            yield put(comparisonActions.setCurrentPage(version, skip));
        }
    } catch (e) {
        console.log(e);
        yield put(comparisonActions.getComparisonError(e));
    }
}
