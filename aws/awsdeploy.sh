docker tag tnz-od-api_api goldiesystems/tnz-od-api_api
docker push goldiesystems/tnz-od-api_api
docker tag tnz-od-api_tnz-compare goldiesystems/tnz-od-api_tnz-compare
docker push goldiesystems/tnz-od-api_tnz-compare
docker tag tnz-od-api_tnz-import goldiesystems/tnz-od-api_tnz-import
docker push goldiesystems/tnz-od-api_tnz-import
scp -i ~/.ssh/tnz-developer.pem aws/docker-compose.aws1.yml ec2-user@ec2-3-84-80-145.compute-1.amazonaws.com:~/docker-compose.yml
scp -i ~/.ssh/tnz-developer.pem aws/aws1.env ec2-user@ec2-3-84-80-145.compute-1.amazonaws.com:~/.env
scp -i ~/.ssh/tnz-developer.pem aws/docker-compose.aws2.yml ec2-user@ec2-54-196-116-91.compute-1.amazonaws.com:~/docker-compose.yml
scp -i ~/.ssh/tnz-developer.pem aws/aws2.env ec2-user@ec2-54-196-116-91.compute-1.amazonaws.com:~/.env

## UI (run from ui folder in project)

npm run deploy
