docker stop $(docker ps -a --filter ancestor=tnz-od-api_api --format="{{.ID}}")
docker container prune --force
docker rmi tnz-od-api_api:latest
docker-compose up -d
