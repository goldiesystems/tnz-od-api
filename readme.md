# TNZ OD API
This project provides a way to call the legacy ("old") Tourism New Zealand (TNZ) Operator Database (OD) APIs and compare the content with the "new" OD equivalent APIs.

There are three parts to the toolkit:

1. Commands to import ( `tnz-import` ) data from the APIs into a local data store and compare ( `tnz-compare` ) to run a comparison of imported data, storing the results in the local data store. These commands take several hours to complete and are intended to be run as batch jobs. Some time can be saved by running imports of old and new data, v1 and v4 as independent processes. v1 and v4 comparisons can also be run independently but, of course, must be run after the respective imports are complete.

2. An API for displaying local data. This is a public RESTful API that returns data in JSON format and can be scanned using standard tools such as Postman.

3. A web application for surfacing the API in a human-friendly format.

## Command Line Apps
To build the command line apps use the root level Makefile
```
$ make build
```

The `tnz-import` command has the following usage:
```
    tnz-import -e [environment] -v [version]

    -e  Environment to import data from. Can be 'old' (default) or 'new'.
    -v  Version of API to use. Can be 1 or 4 (default).
```

## Project Structure
The commands and API are constructed using Go. The project makes use of Go modules and does not need to be on your GOPATH to work with. There are Docker containers for the Go apps and API and the MongoDB data store.

The web UI is a React app. It is not containerised just because it is completely static and can easily be deployed to, say, S3 without needing Docker.

### api
The `api` directory contains a Go service for the RESTful API which essentially reports the contents of the local data store.

### cmd
The `cmd` directory contains the command-line `tnz-import` and `tnz-compare` applications.

### db
The `db` directory contains data access code that is used by the `cmd` and `api` applications.

### internal
Some useful functions shared across the project such as logging and configuration.

### model
The `model` directory contains common data structures. (Config is in `internal`)

### ui
The `ui` directory contains the React application. It uses `create-react-app` so you should be able to
```
$ cd ui
$ npm init
$ npm start
```
to get a localhost dev server running on port 3000.

## Getting Started
The project is designed to run in docker containers. See `docker-compose.yml` for the structure.

You will need to create a `.env` file adjacent to `docker-compose.yml` with the following entries:

```
MONGO_INITDB_ROOT_USERNAME=
MONGO_INITDB_ROOT_PASSWORD=
MONGO_HOST=db
MONGO_PORT=27017
MONGO_USER=
MONGO_PASSWORD=
MONGO_DATABASE=
```
Run
```
$ docker-compose up -d
```
Create a `dbinit.js` file to contain the same credentials set for `MONGO_USER` and `MONGO_PASSWORD` as set in .env. Do not commit this file to git.
```
db = db.getSiblingDB('tnzodapi');
db.createUser(
    {
        user: <secret-user>,
        pwd: <secret-password>,
        roles: [{ role: 'dbOwner', db: 'tnzodapi' }]
    }
);
```
Run
```
$ mongo -u <root-user> -p <root-password> dbinit.js
```
To run a command such as for importing data
```
$ docker-compose run tnz-import -e old -v 1
```
To use the MongoDB cli, connecting to the MongoDB container running on the host machine
```
$ mongo "mongodb://[MONGO_USER]:[MONGO_PASSWORD]@localhost:27017/tnzodapi"
```
(Note that within the Go containers, the MongoDB host presents as "db" so the connection string should contain "db" rather than "localhost")