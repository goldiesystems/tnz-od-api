build:
	go build -a -o ./bin/tnz-compare ./cmd/compare
	go build -a -o ./bin/tnz-import ./cmd/import
	go build -a -o ./bin/api ./api

clean:
	rm -rf ./vendor Gopkg.lock

