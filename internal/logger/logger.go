package logger

import (
	"io"
	"io/ioutil"
	"log"
	"os"
)

// Trace is the logger for tracing (debug) output
var Trace *log.Logger

// Info is the logger for information
var Info *log.Logger

// Warn is the logger for warnings
var Warn *log.Logger

// Error is the logger for errors
var Error *log.Logger

// Init initialises the loggers to write to the specified output writers
func Init(
	traceHandle io.Writer,
	infoHandle io.Writer,
	warnHandle io.Writer,
	errorHandle io.Writer,
) {
	Trace = log.New(traceHandle, "TRACE: ", log.Ldate|log.Ltime|log.Lshortfile)
	Info = log.New(infoHandle, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	Warn = log.New(warnHandle, "WARN: ", log.Ldate|log.Ltime|log.Lshortfile)
	Error = log.New(errorHandle, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

// InitAll sets the writer for all loggers to be the same one
func InitAll(handle io.Writer) {
	Init(handle, handle, handle, handle)
}

// InitFile allows initialisation of loggers to write to the specified files
func InitFile(
	traceFile string,
	infoFile string,
	warnFile string,
	errorFile string,
) error {
	t, err := os.OpenFile(traceFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		return err
	}
	i, err := os.OpenFile(infoFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		return err
	}
	w, err := os.OpenFile(warnFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		return err
	}
	e, err := os.OpenFile(errorFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		return err
	}
	Init(t, i, w, e)
	return nil
}

// InitAllFile sets the output of all loggers to the specified file
func InitAllFile(fileName string) error {
	f, err := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		return err
	}
	InitAll(f)
	return nil
}

// SuppressTrace sets the Trace (debug) logger output to io.Discard so nothing
// sent to this logger will be written
func SuppressTrace() {
	Trace = log.New(ioutil.Discard, "TRACE: ", 0)
}
