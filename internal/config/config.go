package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

// TODO: Replace this with database i/o
const configFile = "conf.json"

// Config is the global application configuration
type Config struct {
	Logdir       string     `json:"logdir"`
	Debug        bool       `json:"debug"`
	PageSize     int        `json:"pageSize"`
	Limit        int        `json:"recordLimit"`
	Delay        int        `json:"delayMilliseconds"`
	Timeout      int        `json:"timeoutSeconds"`
	Retry        int        `json:"retry"`
	Markets      []string   `json:"markets"`
	Regions      []string   `json:"regions"`
	OldEndpoints []Endpoint `json:"oldEndpoints"`
	NewEndpoints []Endpoint `json:"newEndpoints"`
	MapCacheFile string     `json:"mapCacheFile"`
}

// Endpoint holds configuration for TNZ's OD APIs
type Endpoint struct {
	Type       string `json:"type"`
	Token      string `json:"token"`
	BaseURL    string `json:"baseURL"`
	BaseParams string `json:"baseParams"`
}

// Load reads a JSON file and returns the configuration contained therein.
// TODO: Replace this with database i/o
func Load() (Config, error) {

	var config Config

	jsonFile, err := os.Open(configFile)
	if err != nil {
		return config, err
	}

	defer jsonFile.Close()

	byteVal, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return config, err
	}

	json.Unmarshal(byteVal, &config)

	// Fix up templated file names

	if config.Logdir[len(config.Logdir)-1:] != "/" {
		config.Logdir = config.Logdir + "/"
	}

	return config, nil
}

// GetProductEndpoint returns the endpoint configuration to retrieve products
// for the specified environment and version.
func (c Config) GetProductEndpoint(env string, version int) Endpoint {
	var endpoints []Endpoint
	if env == "new" {
		endpoints = c.NewEndpoints
	} else {
		endpoints = c.OldEndpoints
	}
	vString := fmt.Sprintf("v%d", version)
	for _, e := range endpoints {
		if e.Type == vString {
			return e
		}
	}
	return Endpoint{}
}

// GetIDMapEndpoint returns the endpoint configuration to retrieve the mapping
// between core OIDs and listing OIDs for products.
func (c Config) GetIDMapEndpoint() Endpoint {
	for _, e := range c.OldEndpoints {
		if e.Type == "idMap" {
			return e
		}
	}
	return Endpoint{}
}
