package file

import (
	"errors"
	"os/user"
	"strings"
	"time"
)

// ReplaceFilenameWildcards replaces any known templating in the given file
// name with its calculated replacement:
//   ~  at the beginning of the file name is replaced with the user's home
//      directory
//  [T] is replaced with the current date-time in the format YYYYMMDDhhmmss
//
func ReplaceFilenameWildcards(filename string) (string, error) {
	if filename == "" {
		return "", errors.New("Cannot replace wildcards in empty file name")
	}
	newname := filename
	if newname[0] == byte('~') {
		user, err := user.Current()
		if err != nil {
			return newname, err
		}
		newname = strings.Replace(newname, "~", user.HomeDir, -1)
	}

	now := time.Now()
	newname = strings.Replace(newname, "[T]", now.Format("20060102150405"), -1)

	return newname, nil
}
