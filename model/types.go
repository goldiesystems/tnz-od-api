package model

import (
	"encoding/json"
	"strconv"
)

// OID is an integer ID typical in the OD system. Implements an UmarshalJSON
// interface to cope with dodgy JSON.
type OID int

// UnmarshalJSON is the interface used by encoding/json so here it can convert
// either a legitimate int or a string representation of an int into an OID
// (with underlying int type). An empty string returns 0.
func (oid *OID) UnmarshalJSON(b []byte) error {
	if b[0] != '"' {
		return json.Unmarshal(b, (*int)(oid))
	}
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	if s == "" {
		*oid = OID(0)
		return nil
	}
	i, err := strconv.Atoi(s)
	if err != nil {
		return err
	}
	*oid = OID(i)
	return nil
}

// String is a string that copes with dodgy JSON where an unquoted value might
// appear that needs to be coerced into a string.
type String string

// UnmarshalJSON is the interface used by encoding/json so here it can convert
// any value to a string regardless of the inferred type it encounters. Will
// only work on simple types, not maps or slices.
func (str *String) UnmarshalJSON(data []byte) error {
	var b bool
	if err := json.Unmarshal(data, &b); err == nil {
		*str = String(string(data))
		return nil
	}
	var f float64
	if err := json.Unmarshal(data, &f); err == nil {
		*str = String(string(data))
		return nil
	}
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}
	*str = String(s)
	return nil
}

// Equals compares the two Strings allowing for equivalence as parsed bools or
// floats or exact string match.
func (str String) Equals(s2 String) bool {
	if str == s2 {
		return true
	}
	if b1, err := strconv.ParseBool(string(str)); err == nil {
		if b2, err := strconv.ParseBool(string(s2)); err == nil {
			if b1 == b2 {
				return true
			}
		}
	}
	if f1, err := strconv.ParseFloat(string(str), 64); err == nil {
		if f2, err := strconv.ParseFloat(string(s2), 64); err == nil {
			if f1 == f2 {
				return true
			}
		}
	}
	return false
}

// Bool is a boolean that copes with dodgy JSON where a quoted value (string)
// needs to be coerced into a boolean.
type Bool bool

// UnmarshalJSON is the interface used by encoding/json so it can convert a
// boolean-like value into a boolean (or return an error if it can't)
func (bl *Bool) UnmarshalJSON(data []byte) error {
	var b bool
	var err error
	if err = json.Unmarshal(data, &b); err == nil {
		*bl = Bool(b)
		return nil
	}
	var s string
	if err = json.Unmarshal(data, &s); err != nil {
		return err
	}
	if b, err = strconv.ParseBool(s); err != nil {
		return err
	}
	*bl = Bool(b)
	return nil
}

// Float is a floating point number that copes with dodgy JSON where a quoted
// value (string) needs to be coerced into a boolean.
type Float float64

// UnmarshalJSON is the interface used by encoding/json so it can convert a
// float-like string into a float or return an error if it can't. Will convert
// an empty string to zero.
func (fl *Float) UnmarshalJSON(data []byte) error {
	var f float64
	var err error
	if err = json.Unmarshal(data, &f); err == nil {
		*fl = Float(f)
		return nil
	}
	var s string
	if err = json.Unmarshal(data, &s); err != nil {
		return err
	}
	if s == "" {
		*fl = Float(0)
		return nil
	}
	if f, err = strconv.ParseFloat(s, 64); err != nil {
		return err
	}
	*fl = Float(f)
	return nil
}
