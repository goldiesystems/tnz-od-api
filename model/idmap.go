package model

// IDMapper is a structure for mapping "core" OIDs to "listing" OIDs
type IDMapper struct {
	CoreID    int `bson:"coreID"`
	ListingID int `bson:"listingID"`
}
