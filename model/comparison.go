package model

// Comparison holds information on the differences between old and new
// records for the same product
type Comparison struct {
	Key  string         `json:"key" bson:"key"`
	Old  ProductSummary `json:"old" bson:"old"`
	New  ProductSummary `json:"new" bson:"new"`
	Diff []Difference   `json:"diff" bson:"diff"`
}

// Difference records the value from the old API and corresponding value from the new API
type Difference struct {
	FieldTag string `json:"fieldTag" bson:"fieldTag"`
	OldValue string `json:"oldValue" bson:"oldValue"`
	NewValue string `json:"newValue" bson:"newValue"`
}

// HasFault looks internally at the Comparison and returns true if it indicates
// the Old record is different to the New record in some way.
func (c Comparison) HasFault() bool {
	if c.Old.ListingID == 0 || c.New.ListingID == 0 || len(c.Diff) > 0 {
		return true
	}
	return false
}
