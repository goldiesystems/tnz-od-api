package model

// CallSet is a combination of RTO region and market. Used to group calls to
// the TNZ APIs
type CallSet struct {
	Region string `bson:"region"`
	Market string `bson:"market"`
}

// CallSetContains checks to see if the slice of CallSets includes the given
// CallSet
func CallSetContains(css []CallSet, cs CallSet) bool {
	for _, c := range css {
		if c.Region == cs.Region && c.Market == cs.Market {
			return true
		}
	}
	return false
}
