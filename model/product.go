package model

import (
	"encoding/json"
)

// Product is the representation of a tourism business listing
type Product struct {
	RefID                        int                      `json:"refID" bson:"refID"`
	AdultRateLowPeriodLowPrice   String                   `json:"adult_rate_low_period_low_price" bson:"adult_rate_low_period_low_price"`
	MailingPostcode              String                   `json:"mailing_postcode" bson:"mailing_postcode"`
	ProximityToCoach             String                   `json:"proximity_to_coach" bson:"proximity_to_coach"`
	ProximityToTown              String                   `json:"proximity_to_town" bson:"proximity_to_town"`
	ODAccountOID                 OID                      `json:"od_account_o_id" bson:"od_account_o_id"`
	RowNumber                    int                      `json:"rownumber" bson:"rownumber"`
	ListingTypes                 []string                 `json:"listing_types" bson:"listing_types"`
	OperationalHours             String                   `json:"operational_hours" bson:"operational_hours"`
	WebsiteLinkIsPublic          int                      `json:"website_link_is_public" bson:"website_link_is_public"`
	AdultRateHighPeriodHighPrice String                   `json:"adult_rate_high_period_high_price" bson:"adult_rate_high_period_high_price"`
	Parking                      String                   `json:"parking" bson:"parking"`
	Assets                       AssetSet                 `json:"assets" bson:"assets"`
	Market                       string                   `json:"market" bson:"market"`
	HighPeriodStart              String                   `json:"high_period_start" bson:"high_period_start"`
	ModifiedDate                 string                   `json:"modified_date" bson:"modified_date"`
	RegionName                   string                   `json:"regionname" bson:"regionname"`
	MailingStateProvince         string                   `json:"mailing_stateprovince" bson:"mailing_stateprovince"`
	Tags                         []string                 `json:"tags" bson:"tags"`
	WebsiteLink                  string                   `json:"website_link" bson:"website_link"`
	BookingEmail                 string                   `json:"booking_email" bson:"booking_email"`
	MailingCityName              string                   `json:"mailing_cityname" bson:"mailing_cityname"`
	LowPeriodEnd                 String                   `json:"low_period_end" bson:"low_period_end"`
	Postcode                     String                   `json:"postcode" bson:"postcode"`
	ListingNameKey               string                   `json:"listing_name_key" bson:"listing_name_key"`
	Suburb                       string                   `json:"suburb" bson:"suburb"`
	UniqueID                     OID                      `json:"unique_id" bson:"unique_id"`
	LanguagesSpoken              []string                 `json:"languages_spoken" bson:"languages_spoken"`
	ChildRateHighPeriodHighPrice String                   `json:"child_rate_high_period_high_price" bson:"child_rate_high_period_high_price"`
	Freephone                    String                   `json:"freephone" bson:"freephone"`
	ChildRateHighPeriodLowPrice  String                   `json:"child_rate_high_period_low_price" bson:"child_rate_high_period_low_price"`
	QualmarkV2Rating             QualmarkV2               `json:"qualmark_v2_rating" bson:"qualmark_v2_rating"`
	AdultRateHighPeriodLowPrice  String                   `json:"adult_rate_high_period_low_price" bson:"adult_rate_high_period_low_price"`
	BusinessType                 string                   `json:"business_type" bson:"business_type"`
	CancellationPolicy           String                   `json:"cancellation_policy" bson:"cancellation_policy"`
	CountryName                  string                   `json:"countryname" bson:"countryname"`
	MailingLUCityOID             OID                      `json:"mailing_lu_city_o_id" bson:"mailing_lu_city_o_id"`
	FaxIsPublic                  int                      `json:"fax_ispublic" bson:"fax_ispublic"`
	Awards                       []string                 `json:"awards" bson:"awards"`
	PricingHasHighLow            int                      `json:"pricing_has_highlow" bson:"pricing_has_highlow"`
	ODListingOID                 OID                      `json:"od_listing_o_id" bson:"od_listing_o_id"`
	Street                       String                   `json:"street" bson:"street"`
	AllSocialProfiles            map[string]String        `json:"all_social_profiles" bson:"all_social_profiles"`
	Rooms                        String                   `json:"rooms" bson:"rooms"`
	MinimumAge                   String                   `json:"minimum_age" bson:"minimum_age"`
	LanguagesWritten             []string                 `json:"languages_written" bson:"languages_written"`
	NameOfListing                string                   `json:"nameoflisting" bson:"nameoflisting"`
	City                         string                   `json:"city" bson:"city"`
	WeatherConsiderations        String                   `json:"weather_considerations" bson:"weather_considerations"`
	Mobile                       String                   `json:"mobile" bson:"mobile"`
	CityName                     string                   `json:"cityname" bson:"cityname"`
	RefIDInt                     String                   `json:"ref_id_int" bson:"ref_id_int"`
	DOCConcessionID              String                   `json:"doc_concession_id" bson:"doc_concession_id"`
	MailingStreet                String                   `json:"mailing_street" bson:"mailing_street"`
	StateProvince                string                   `jsong:"stateprovince" bson:"stateprovince"`
	Email                        string                   `json:"email" bson:"email"`
	ListingDescription           String                   `json:"listing_description" bson:"listing_description"`
	EmailIsPublic                int                      `json:"email_ispublic" bson:"email_ispublic"`
	FreephoneIsPublic            int                      `json:"freephone_ispublic" bson:"freephone_ispublic"`
	ProfileImageOID              OID                      `json:"profile_image_o_id" bson:"profile_image_o_id"`
	BookingPhone                 String                   `json:"booking_phone" bson:"booking_phone"`
	Amenities                    []string                 `json:"amenities" bson:"amenities"`
	IsISite                      int                      `json:"is_isite" bson:"is_isite"`
	MailingSuburb                string                   `json:"mailing_suburb" bson:"mailing_suburb"`
	ChildRateLowPeriodHighPrice  String                   `json:"child_rate_low_period_high_price" bson:"child_rate_low_period_high_price"`
	MailingCountryName           string                   `json:"mailing_countryname" bson:"mailing_countryname"`
	MarketLabel                  string                   `json:"market_label" bson:"market_label"`
	BookingLink                  string                   `json:"booking_link" bson:"booking_link"`
	Deals                        []map[string]interface{} `json:"deals" bson:"deals"`
	ProximityToAirport           String                   `json:"proximity_to_airport" bson:"proximity_to_airport"`
	Latitude                     Float                    `json:"latitude" bson:"latitude"`
	LUCountryOID                 OID                      `json:"lu_country_o_id" bson:"lu_country_o_id"`
	LowPeriodState               string                   `json:"low_period_start" bson:"low_period_start"`
	ODBusinessTypeOID            OID                      `json:"od_businesstype_o_id" bson:"od_businesstype_o_id"`
	Longitude                    Float                    `json:"longitude" bson:"longitude"`
	Phone                        String                   `json:"phone" bson:"phone"`
	LURegionOID                  OID                      `json:"lu_region_o_id" bson:"lu_region_o_id"`
	OnlineProfiles               []OnlineProfile          `json:"online_profiles" bson:"online_profiles"`
	MailingRegionName            string                   `json:"mailing_regionname" bson:"mailing_regionname"`
	Conditions                   String                   `json:"conditions" bson:"conditions"`
	MailingIsPublic              int                      `json:"mailing_ispublic" bson:"mailing_ispublic"`
	OperatingMonths              String                   `json:"operating_months" bson:"operating_months"`
	IsKiwiSpecialist             int                      `json:"is_kiwi_specialist" bson:"is_kiwi_specialist"`
	MailingLURegionOID           OID                      `json:"mailing_lu_region_o_id" bson:"mailing_lu_region_o_id"`
	ChildRateAge                 String                   `json:"child_rate_age" bson:"child_rate_age"`
	ActivityDuration             String                   `json:"activity_duration" bson:"activity_duration"`
	ChildRateLowPeriodLowPrice   String                   `json:"child_rate_low_period_low_price" bson:"child_rate_low_period_low_price"`
	MailingCity                  string                   `json:"mailing_city" bson:"mailing_city"`
	PhoneIsPublic                int                      `json:"phone_ispublic" bson:"phone_ispublic"`
	AdultRateLowPeriodHighPrice  String                   `json:"adult_rate_low_period_high_price" bson:"adult_rate_low_period_high_price"`
	IsTour                       int                      `json:"is_tour" bson:"is_tour"`
	UsePhysicalAddress           int                      `json:"use_physical_address" bson:"use_physical_address"`
	MaxCapacity                  String                   `json:"max_capacity" bson:"max_capacity"`
	ListingSummary               String                   `json:"listing_summary" bson:"listing_summary"`
	LogoImageAssetOID            OID                      `json:"logo_image_asset_o_id" bson:"logo_image_asset_o_id"`
	MailingLUCountryOID          OID                      `json:"mailing_lu_country_o_id" bson:"mailing_lu_country_o_id"`
	QualmarkRatings              map[string]string        `json:"qualmark_ratings" bson:"qualmark_ratings"`
	OtherCharges                 String                   `json:"other_charges" bson:"other_charges"`
	ODListingMarketOID           OID                      `json:"od_listing_market_o_id" bson:"od_listing_market_o_id"`
	Fax                          String                   `json:"fax" bson:"fax"`
	LUCityOID                    OID                      `json:"lu_city_o_id" bson:"lu_city_o_id"`
	HighPeriodEnd                String                   `json:"high_period_end" bson:"high_period_end"`
	Message                      string                   `json:"message" bson:"message"`
}

// AssetSet is a set of image and video assets associated with a product
type AssetSet struct {
	Logo    map[string]Asset `json:"logo" bson:"logo"`
	Main    map[string]Asset `json:"main" bson:"main"`
	Gallery map[string]Asset `json:"gallery" bson:"gallery"`
}

// Asset is an image or video
type Asset struct {
	Credit      String          `json:"credit" bson:"credit"`
	Market      string          `json:"market" bson:"market"`
	Exists      Bool            `json:"exists" bson:"exists"`
	Longitude   Float           `json:"longitude" bson:"longitude"`
	Width       json.Number     `json:"width" bson:"width"`
	Instances   []AssetInstance `json:"instances" bson:"instances"`
	UniqueID    OID             `json:"unique_id" bson:"unique_id"`
	Height      json.Number     `json:"height" bson:"height"`
	OID         OID             `json:"o_id" bson:"o_id"`
	URL         string          `json:"url" bson:"url"`
	Description String          `json:"description" bson:"description"`
	AssetType   string          `json:"asset_type" bson:"asset_type"`
	Label       String          `json:"label" bson:"label"`
	Caption     String          `json:"caption" bson:"caption"`
	Latitude    Float           `json:"latitude" bson:"latitude"`
	TypeOID     OID             `json:"type_o_id" bson:"type_o_id"`
}

// AssetInstance is a copy of an image or video that has specific size
type AssetInstance struct {
	ProviderLabel string      `json:"provider_label" bson:"provider_label"`
	Format        string      `json:"format" bson:"format"`
	Width         json.Number `json:"width" bson:"width"`
	Height        json.Number `json:"height" bson:"height"`
	URL           string      `json:"url" bson:"url"`
}

// QualmarkV2 is version 2 Qualmark rating information
type QualmarkV2 struct {
	AwardSegment string `json:"awardSegment" bson:"awardSegment"`
	Exists       Bool   `json:"exists" bson:"exists"`
	Rating       string `json:"rating" bson:"rating"`
	Grade        string `json:"grade" bson:"grade"`
	AwardSector  string `json:"awardSector" bson:"awardSector"`
}

// OnlineProfile has a service provider and specific URL for a product's online
// presence on that platform (e.g. Facebook page)
type OnlineProfile struct {
	ProfileType string `json:"profile_type" bson:"profile_type"`
	ProfileData String `json:"profile_data" bson:"profile_data"`
}

// ProductSummary gives some very minimal information to help visually
// recognise a product
type ProductSummary struct {
	ListingID    int    `json:"listingID" bson:"listingID"`
	CoreID       int    `json:"coreID" bson:"coreID"`
	Market       string `json:"market" bson:"market"`
	Name         string `json:"name" bson:"name"`
	ThumbnailURL string `json:"thumbnail" bson:"thumbnail"`
	Message      string `json:"message" bson:"message"`
}

// ProductError records a problem encountered while processing a product
type ProductError struct {
	ODListingOID  int    `json:"od_listing_o_id" bson:"od_listing_o_id"`
	Market        string `json:"market" bson:"market"`
	NameOfListing string `json:"nameoflisting" bson:"nameoflisting"`
	Message       string `json:"message" bson:"message"`
	APIURL        string `json:"apiurl" bson:"apiurl"`
}

// CreateSummary finds the appropriate fields in the map representation of
// a product. Will indicate whether all fields were found successfully.
func CreateSummary(p Product, environment string) ProductSummary {
	var coreID, listingID int
	if environment == "old" {
		coreID = int(p.ODListingOID)
		listingID = p.RefID
	} else {
		coreID = 0
		listingID = int(p.ODListingOID)
	}
	s := ProductSummary{
		ListingID: listingID,
		CoreID:    coreID,
		Market:    p.Market,
		Name:      p.NameOfListing,
		Message:   p.Message,
	}

	if len(p.Assets.Main) > 0 {
		for _, mainAsset := range p.Assets.Main {
			if len(mainAsset.Instances) > 0 {
				for _, instance := range mainAsset.Instances {
					if instance.Format == "thumbnail" {
						s.ThumbnailURL = instance.URL
						break
					}
				}
			}
			if s.ThumbnailURL != "" {
				break
			}
		}
	}

	return s
}

// CreateProductError copies summary information from the product to a record
// of known data issues.
func CreateProductError(p Product, url string) ProductError {
	return ProductError{
		ODListingOID:  int(p.ODListingOID),
		Market:        p.Market,
		NameOfListing: p.NameOfListing,
		Message:       p.Message,
		APIURL:        url,
	}
}

// HasKnownError returns true if the data contains a problem according to the
// internal hard-coded business rules.
func (p Product) HasKnownError(market string) bool {
	if p.ODListingOID <= 0 {
		return true
	}
	if p.Message != "" {
		return true
	}
	if p.Market != market {
		return true
	}
	return false
}
